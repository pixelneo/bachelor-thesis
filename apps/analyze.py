#!/usr/bin/env python3
import numpy as np
import pandas as pd
from credit_card_dataset import CreditCardDataset
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

if __name__ == '__main__':
    dataset = CreditCardDataset('data.nosync/train.npy')
    data = dataset.get_raw()[:5]

    # var = np.dstack((np.arange(len(data[0])),np.var(data, axis=0)))[0]
    # var = var[var[:,1].argsort()[::-1]]  # sort by 2 arg reversed
    print('---- Variance of features ----')
    # print('\n'.join(['{}: {}'.format(i[0],i[1]) for i in var]))

    # df = pd.DataFrame(data)
    # print(df.mean().to_latex())
    print(data.shape)
    df = pd.DataFrame(data)
    print(df.head().to_latex(columns=[0,1,len(data[0])-3,len(data[0])-2]))
    # plt.plot(np.arange(data[0].size), pca.explained_variance_)
    #plt.plot(np.arange(data[0].size), np.mean(pca.components_, axis=1))
    # plt.show()
