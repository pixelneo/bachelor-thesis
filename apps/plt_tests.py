#!/usr/bin/env python3
import numpy as np

import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt
import pandas as pd

if __name__ == '__main__':

    svm = np.array([0.44693, 0.43802, 0.3893 , 0.4695 , 0.46602, 0.528  , 0.36701, 0.42117, 0.38958, 0.32564, 0.33626, 0.31682, 0.37445, 0.31293, 0.51   , 0.48965, 0.40573, 0.35351, 0.34336, 0.32071, 0.35628, 0.42711, 0.50422, 0.38239, 0.42661, 0.4177 , 0.3853 , 0.42902, 0.59571])
    ano = np.array([0.537094,0.478667,0.350873,0.489517,0.402794,0.505925,0.524242,0.470421,0.364057,0.412344,0.438417,0.442282,0.421872,0.484899,0.526872,0.449714,0.438730,0.465116,0.476113,0.411600,0.615832,0.400447,0.456688,0.523920,0.587660,0.387037,0.486817,0.473228,0.390600])
    data = np.transpose(np.stack((ano, svm)))
    df = pd.DataFrame(data)
    df = df.sort_values([0,1], ascending=False)
    print(np.average(ano))
    print(np.average(svm))
    print(df)

    # print(data.shape)

    plt.rc('font', family='serif')
    f = plt.figure(figsize=(7,4))
    x = np.arange(29)
    poly = np.polyfit(x, df[0], 1)
    func = np.poly1d(poly)
    poly2 = np.polyfit(x, df[1], 1)
    func2 = np.poly1d(poly2)
    print(np.corrcoef(df[0], df[1]))
    print(np.corrcoef(func(df[0]), func2(df[1])))

    plt.plot(x, func(x), '--', color='#4040D0', linewidth=0.95)
    plt.plot(x, func2(x), '--', color='#40D040', linewidth=0.95)
    plt.plot(x, df[1], color='green', label='OC-SVM', marker='^', linewidth=0.8)
    plt.plot(x, df[0], color='navy', label='AnoWGAN+e', marker='o', linewidth=0.8)

    plt.xlabel('Test')
    plt.ylabel('AUPRC')
    # plt.axis((-0.05,1.05,-0.05,1.05))
    plt.legend(loc="upper right")
    plt.title('AUPRC of each test of AnoWGAN+e and OC-SVM')

    f.savefig("foo_scores.pdf", bbox_inches='tight')

    plt.show()

