#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import math

def func(x):
    return np.log(1 + np.exp(2*x + 2))/2 - 1

def lrelu(x):
    return np.maximum(0.2*x, x)
    # return 0.2*x if x < 0 else x

if __name__=='__main__':

    x = np.arange(-1.0, 1.0, 0.01)

    plt.rc('font', family='serif')
    f = plt.figure(figsize=(6,3))
    # fig, ax = plt.subplots()
    # plt.grid(True) 
    plt.axhline(0,linewidth=0.5, color='black')
    plt.axvline(0,linewidth=0.5, color='black')

    plt.plot(x, lrelu(x), color='black', label=r'Leaky ReLU $\alpha = 0.2$')

    plt.xlabel(r'$x$')
    plt.ylabel(r'$Leaky ReLU(x; 0.2)$')
    plt.legend(loc="lower right")
    plt.title('Graph of Leaky ReLU')

    f.savefig("foo_act.pdf", bbox_inches='tight')

    plt.show()

