#!/usr/bin/env python3
import numpy as np
import pandas as pd
from credit_card_dataset import CreditCardDataset
from sklearn.decomposition import PCA
import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt

if __name__ == '__main__':
    np.random.seed(41)
    plt.rc('font', family='serif')
    plt.rc('image', cmap='seismic')
    f = plt.figure()

    dataset = CreditCardDataset('data.nosync/train.npy')
    data = dataset.get_raw()

    df = pd.DataFrame(data)


    corr = df.corr()
    ax = f.add_subplot(111)
    x = ax.matshow(corr, vmin=-1, vmax=1)
    f.colorbar(x)
    ticks = np.arange(0,28,1)
    names = [str(x) if x%2 == 0 else '' for x in range(1,29)]
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.set_xticklabels(names)
    ax.set_yticklabels(names)

    plt.title('Correlation of features')

    f.savefig("corr.pdf", bbox_inches='tight')
    plt.show()

