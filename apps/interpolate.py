#!/usr/bin/env python3
import numpy as np
from utils import *

import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt

if __name__ == '__main__':

    plt.rc('font', family='serif')
    f = plt.figure(figsize=(7,4.5))

    # Our model encoder

    x = np.arange(1,0, -0.001)
    data = []
    for i in range(29):
        file = np.load('evals.nosync/prec_rec_encoder{}.npz'.format(i))
        data.extend(file['arr_0'])

    # data = np.load('evals/prec_rec_encoder.npz')
    # prs = np.average(pr)

    ys = []
    for i, (prec, rec) in enumerate(data):
        poly = np.polyfit(rec, prec, 10)
        y = np.polyval(poly, x)
        y = np.clip(y, 0,1)
        ys.append(y)

    ys = np.array(ys)
    mid = np.average(ys, axis=0)
    mid2 = mid
    plt.plot(x, mid, color='navy', label='AnoWGAN+e (auprc = {:.4f})'.format(0.3823))



    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.axis((-0.05,1.05,-0.05,1.05))
    plt.legend(loc="upper right")
    plt.title('Averaged PR-curves')

    f.savefig("foo_compare.pdf", bbox_inches='tight')


    plt.show()
    exit()
    # SVM 
    model = 'svm_'

    x = np.arange(1,0, -0.001)
    data = []
    for i in range(29):
        file = np.load('evals/svm_prec_rec{}.npz'.format(i))
        data.extend(file['arr_0'])


    ys = []
    for i, (prec, rec) in enumerate(data):
        poly = np.polyfit(rec, prec, 10)
        y = np.polyval(poly, x)
        y = np.clip(y, 0,1)
        ys.append(y)

    ys = np.array(ys)
    mid = np.average(ys, axis=0)

    # zjisteni protnuti
    # for i in range(900, 700, -1):
        # print('{:.3f} {} {}'.format((1000-i)/1000,mid2[i],mid[i]))

    plt.plot(x, mid, color='green', label='OC-SVM (auprc = {:.4f})'.format(0.4113))


    # Isolation F
    model = 'if_'

    x = np.arange(1,0, -0.001)
    data = []
    for i in range(29):
        file = np.load('evals/if_prec_rec{}.npz'.format(i))
        data.extend(file['arr_0'])

    ys = []
    for i, (prec, rec) in enumerate(data):
        poly = np.polyfit(rec, prec, 10)
        y = np.polyval(poly, x)
        y = np.clip(y, 0,1)
        ys.append(y)

    ys = np.array(ys)
    mid = np.average(ys, axis=0)

    plt.plot(x, mid, color='fuchsia', label='Isolation Forest (auprc = {:.4f})'.format(0.1827))




    # Our model
    model = ''

    x = np.arange(1,0, -0.001)
    data = []
    for i in range(29):
        file = np.load('evals/prec_rec{}.npz'.format(i))
        data.extend(file['arr_0'])

    ys = []
    for i, (prec, rec) in enumerate(data):
        poly = np.polyfit(rec, prec, 10)
        y = np.polyval(poly, x)
        y = np.clip(y, 0,1)
        ys.append(y)

    ys = np.array(ys)
    mid = np.average(ys, axis=0)
    
    plt.plot(x, mid, color='red', label='AnoWGAN–e (auprc = {:.4f})'.format(0.2706))
    # # LOF 
    # model = 'lof_'

    # x = np.arange(1,0, -0.001)
    # data = np.load('evals/{}prec_rec.npz'.format(model))
    # pr = np.load('evals/{}prec_au.npy'.format(model))
    # prs = np.average(pr)

    # ys = []
    # for i, (prec, rec) in enumerate(data['arr_0']):
        # poly = np.polyfit(rec, prec, 10)
        # y = np.polyval(poly, x)
        # y = np.clip(y, 0,1)
        # ys.append(y)

    # ys = np.array(ys)
    # mid = np.average(ys, axis=0)
    
    # plt.plot(x, mid, color='red', label='LOF (avg. area = {:.3f})'.format(prs))



    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.axis((-0.05,1.05,-0.05,1.05))
    plt.legend(loc="upper right")
    plt.title('Averaged PR-curves')

    f.savefig("foo_compare.pdf", bbox_inches='tight')

    plt.show()

