#!/usr/bin/env python3 
import numpy as np
import tensorflow as tf
from importlib import reload

from credit_card_dataset2 import CreditCardDataset
"""
This file is based on gan.py template by Milan Straka https://github.com/ufal/npfl114/blob/master/labs/10/gan.py
However it has been altered.

License: CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
"""
class Network:
    def __init__(self, threads, seed=42):
        graph = tf.Graph()
        graph.seed = seed
        self.dim = 30
        self.session = tf.Session(graph = graph, config=tf.ConfigProto(inter_op_parallelism_threads=threads,
                                                                       intra_op_parallelism_threads=threads))

    def construct(self, args):
        self.z_dim = args.z_dim

        with self.session.graph.as_default():

            # Inputs
            self.inputs = tf.placeholder(tf.float32, [None, self.dim])
            self.z = tf.placeholder(tf.float32, [None, self.z_dim])
            inputs = self.inputs
            # inputs = self.inputs + tf.random_normal(shape=tf.shape(self.inputs), mean=0.0, stddev=0.3, dtype=tf.float32)

            # Generator
            def generator(z):
                hidden_layer = tf.layers.dense(z, 32, activation=tf.nn.relu)
                hidden_layer = tf.layers.dense(hidden_layer, 64, activation=tf.nn.relu)
                hidden_layer = tf.layers.dense(hidden_layer, 128, activation=tf.nn.relu)
                result = tf.layers.dense(hidden_layer, self.dim, activation=tf.nn.sigmoid)
                return result

            with tf.variable_scope("generator"):
                self.generated = generator(self.z)

            # Discriminator
            def discriminator(data):
                hidden_layer = tf.layers.dense(data, 128, activation=tf.nn.leaky_relu)
                tf.layers.dropout(hidden_layer, 0.3)
                hidden_layer = tf.layers.dense(hidden_layer, 64, activation=tf.nn.leaky_relu)
                tf.layers.dropout(hidden_layer, 0.3)
                hidden_layer = tf.layers.dense(hidden_layer, 32, activation=tf.nn.leaky_relu)
                tf.layers.dropout(hidden_layer, 0.3)
                hidden_layer = tf.layers.dense(hidden_layer, 16, activation=tf.nn.leaky_relu)
                tf.layers.dropout(hidden_layer, 0.3)
                logit = tf.layers.dense(hidden_layer, 1)
                return logit[:, 0]

            with tf.variable_scope("discriminator"):
                discriminator_logit_real = discriminator(inputs)

            with tf.variable_scope("discriminator", reuse = True):
                discriminator_logit_fake = discriminator(self.generated)
            # Losses
            self.discriminator_loss = (
                tf.losses.sigmoid_cross_entropy(tf.ones_like(discriminator_logit_real), discriminator_logit_real, label_smoothing=0.2) +
                tf.losses.sigmoid_cross_entropy(tf.zeros_like(discriminator_logit_fake), discriminator_logit_fake, label_smoothing=0.2)
            )
            self.generator_loss = tf.losses.sigmoid_cross_entropy(tf.ones_like(discriminator_logit_fake), discriminator_logit_fake, label_smoothing=0.2)

            # Training
            global_step = tf.train.create_global_step()
            self.discriminator_training = tf.train.AdamOptimizer(args.d_lr).minimize(
                self.discriminator_loss, var_list=tf.global_variables("discriminator"))
            self.generator_training = tf.train.AdamOptimizer(args.g_lr).minimize(
                self.generator_loss, global_step=global_step, var_list=tf.global_variables("generator"))

            # Summaries
            discriminator_accuracy = tf.reduce_mean(tf.to_float(tf.concat([
                tf.greater(discriminator_logit_real, 0), tf.less(discriminator_logit_fake, 0)], axis=0)))
            d_a_tp = tf.reduce_mean(tf.to_float(tf.greater(discriminator_logit_real, 0)))
            d_a_fp = tf.reduce_mean(tf.to_float(tf.less(discriminator_logit_fake, 0)))

            self.d_a =d_a_tp 
            self.d_b =d_a_fp 
            summary_writer = tf.contrib.summary.create_file_writer(args.logdir, flush_millis=10 * 1000)
            with summary_writer.as_default(), tf.contrib.summary.record_summaries_every_n_global_steps(100):
                self.discriminator_summary = [tf.contrib.summary.scalar("gan/discriminator_loss", self.discriminator_loss),
                                              tf.contrib.summary.scalar("gan/discriminator_accuracy", discriminator_accuracy)]
                self.generator_summary = tf.contrib.summary.scalar("gan/generator_loss", self.generator_loss)

            self.generated_image_data = tf.placeholder(tf.float32, [None, None, 1])
            with summary_writer.as_default(), tf.contrib.summary.always_record_summaries():
                self.generated_image_summary = tf.contrib.summary.image("gan/generated_image",
                                                                        tf.expand_dims(self.generated_image_data, axis=0))

            # Initialize variables
            self.session.run(tf.global_variables_initializer())
            with summary_writer.as_default():
                tf.contrib.summary.initialize(session=self.session, graph=self.session.graph)

    def sample_z(self, batch_size):
        return np.random.uniform(-1, 1, size=[batch_size, self.z_dim])

    def get_discriminator_loss(self, data):
        loss = self.session.run([self.discriminator_summary, self.discriminator_loss],
                                {self.inputs: data, self.z: self.sample_z(len(data))})[-1]
        return loss

    def train_discriminator(self, data):
        loss = self.session.run([self.discriminator_training, self.discriminator_summary, self.discriminator_loss],
                                {self.inputs: data, self.z: self.sample_z(len(data))})[-1]
        return loss

    def train_generator(self, batch_size):
        loss = self.session.run([self.generator_training, self.generator_summary, self.generator_loss],
                                 {self.z: self.sample_z(batch_size)})[-1]
        return loss
    def test_disc(self, data):
        acc = self.session.run([self.d_a, self.d_b], {self.inputs: data, self.z:self.sample_z(len(data))})
        return acc

if __name__ == "__main__":
    import argparse
    import datetime
    import os
    import re

    # Fix random seed
    np.random.seed(42)

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size", default=50, type=int, help="Batch size.")
    parser.add_argument("--epochs", default=100, type=int, help="Number of epochs.")
    parser.add_argument("--z_dim", default=16, type=int, help="Dimensionality of latent space.")
    parser.add_argument("--threads", default=4, type=int, help="Maximum number of threads to use.")
    parser.add_argument("--k", default=4, type=int, help="Discriminator iterations.")
    parser.add_argument("--d_lr", default=0.00001, type=float, help="Discriminator learning rate.")
    parser.add_argument("--g_lr", default=0.00001, type=float, help="Generator learning rate.")
    args = parser.parse_args()

    # Create logdir name
    args.logdir = "logs/{}-{}-{}".format(
        os.path.basename(__file__),
        datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S"),
        ",".join(("{}={}".format(re.sub("(.)[^_]*_?", r"\1", key), value) for key, value in sorted(vars(args).items())))
    )
    if not os.path.exists("logs"): os.mkdir("logs")

    # Load dataset
    train = CreditCardDataset('./data.nosync/train.npy')      # TODO: uncomment
    dev = CreditCardDataset('./data.nosync/devel.npy')
    test = CreditCardDataset('./data.nosync/test.npy')

    # Construct the network
    network = Network(threads=args.threads)
    network.construct(args)

    k = 0
    # Train
    for i in range(args.epochs):
        loss = 0
        while not train.epoch_finished:
            k +=1 
            data, _ = train.next_batch(args.batch_size)
            disc_loss = network.get_discriminator_loss(data)
            if (disc_loss > 4.) or (disc_loss > 2. and k > 20000 and k % 5 == 0) or (disc_loss > 1.5 and k > 35000 and k % 10 == 0) or (disc_loss > 1.2 and k > 50000 and k % 20 == 0): 
                loss = network.train_discriminator(data)

            if k%500 == 0:
                print(k)
                data = dev.get_normal_batch(120)

                a, b = network.test_disc(data)
                print('{:.4f}'.format(a))
                print('{:.4f}'.format(b))
            for _ in range(args.k):
                loss += network.train_generator(50)
        # print("{:.2f}".format(loss))

