#!/usr/bin/env python3

import numpy as np
from sklearn import ensemble, metrics
from credit_card_dataset import CreditCardDataset, CrossValidator

import pickle
import argparse

def sigmoid2(x):
    # return x
    return 1 / (1 + np.exp(-x))

def compute_precision_recall(y, scores, file=None):
    precision, recall, thresholds = metrics.precision_recall_curve(y, scores, pos_label=1)
    pr = metrics.average_precision_score(y, scores, pos_label=1)
    if file:
        np.savez(file, precision=precision, recall=recall, thresholds=thresholds, pr=pr)
        return pr
    else:
        return (precision, recall, thresholds, pr)

def run_eval2(classifier, dev):
    prs = []
    prec_rec = []
    anom = dev.get_all_anomalous()
    data_normal = dev.get_all_normal()
    predicted_normal = - classifier.decision_function(data_normal)
    for i in range(26):
        data_anom = anom[i*17:i*17 + 17]
        predicted_anom = - classifier.decision_function(data_anom)

        gold = np.concatenate((np.zeros(len(data_normal)), np.ones(len(data_anom)))) 
        pred = np.concatenate((sigmoid2(predicted_normal), sigmoid2(predicted_anom)))

        prec, rec, th, pr2 = compute_precision_recall(gold, pred) 
        prs.append(pr2)
        prec_rec.append((prec, rec))
    print(np.average(prs))
    return np.array(prs), prec_rec


if __name__ == '__main__':
    np.random.seed(41)
    np.set_printoptions(threshold=np.inf)
    parser = argparse.ArgumentParser()
    parser.add_argument("--train", action='store_true', help="Train the classifier.")
    parser.add_argument("--cross", action='store_true', help="Run cross validation.")
    # parser.add_argument("--test", action='store_true', help="Test the classifier.")
    args = parser.parse_args()
    
    classifier = ensemble.IsolationForest(n_estimators=50, max_samples=10000, contamination=0.0018, max_features=1.0, bootstrap=False, random_state=41, verbose=0,behaviour="new", n_jobs=4)
                                          # contamination=0.002, n_jobs=4)
    
    if args.train:
        train = CreditCardDataset.from_file('data.nosync/train.npy')

        data  = train.get_all_normal()
        classifier.fit(data)
        with open('data.nosync/if_model.pickle', 'wb') as f:
            pickle.dump(classifier, f)

    elif args.cross:
        prs = []
        for i in range(10):
            train_d = CreditCardDataset.from_file('./data.nosync/train{}.npy'.format(i))
            test_d = CreditCardDataset.from_file('./data.nosync/test{}.npy'.format(i))
            classifier = ensemble.IsolationForest(n_estimators=100, max_samples='auto', contamination=0.01, max_features=1.0, bootstrap=False, n_jobs=-1, random_state=42, verbose=0,behaviour="new")

            data = train_d.get_all_normal()
            classifier.fit(data)

            pr, prec_rec = run_eval2(classifier, test_d)
            if i==1:
                np.savez('evals/if_prec_rec.npz', prec_rec) 
            prs.append(pr)
        print('\n\n\n-------- Result of crossvalidation --------- \n')
        print('Average AUPRC = {}'.format(np.average(np.array(prs))))
        print('Var = {}'.format(np.var(np.array(prs))))
        print('Std = {}'.format(np.std(np.array(prs))))



    else:
        dev = CreditCardDataset.from_file('data.nosync/test.npy', shuffle=False)
        with open('data.nosync/if_model.pickle', 'rb') as f:
            classifier = pickle.load(f)

        prs = []
        prec_rec = []
        anom = dev.get_all_anomalous()
        data_normal = dev.get_all_normal()[:9735]
        predicted_normal = - classifier.decision_function(data_normal)
        for i in range(26):
            data_anom = anom[i*17:i*17 + 17]


            predicted_anom = - classifier.decision_function(data_anom)

            gold = np.concatenate((np.zeros(len(data_normal)), np.ones(len(data_anom)))) 
            pred = np.concatenate((sigmoid2(predicted_normal), sigmoid2(predicted_anom)))

            prec, rec, th, pr2 = compute_precision_recall(gold, pred) 
            prs.append(pr2)
            prec_rec.append((prec, rec))
        print(np.average(prs))
        np.savez('evals/if_prec_rec.npz', prec_rec)
        np.save('evals/if_prec_au.np', prs)


        exit()

        plt.rc('font', family='serif')
        f = plt.figure()
        plt.plot(rec, prec, color='black', label='Precision Recall curve (area = {:.3f})'.format(pr2))

        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.legend(loc="lower left")
        plt.title('Precision Recall curve of IsolationForest')

        f.savefig("foo_if.pdf", bbox_inches='tight')

        plt.show()
