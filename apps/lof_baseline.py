#!/usr/bin/env python3

import numpy as np
from sklearn import neighbors, metrics
from credit_card_dataset import CreditCardDataset

import pickle
import argparse
from matplotlib import pyplot as plt

def sigmoid2(x):
    # return x
    return 1 / (1 + np.exp(-x))

def compute_precision_recall(y, scores, file=None):
    precision, recall, thresholds = metrics.precision_recall_curve(y, scores, pos_label=1)
    pr = metrics.average_precision_score(y, scores, pos_label=1)
    if file:
        np.savez(file, precision=precision, recall=recall, thresholds=thresholds, pr=pr)
        return pr
    else:
        return (precision, recall, thresholds, pr)


if __name__ == '__main__':
    np.random.seed(41)
    np.set_printoptions(threshold=np.inf)
    parser = argparse.ArgumentParser()
    parser.add_argument("--train", action='store_true', help="Train the classifier.")
    # parser.add_argument("--test", action='store_true', help="Test the classifier.")
    args = parser.parse_args()
    
    classifier = neighbors.LocalOutlierFactor(novelty=True, contamination=0.002, n_jobs=4)
    
    if args.train:
        train = CreditCardDataset('data.nosync/train.npy')

        data  = train.get_all_normal()[:50000]
        classifier.fit(data)
        with open('data.nosync/lof_model.pickle', 'wb') as f:
            pickle.dump(classifier, f)
    else:
        dev = CreditCardDataset('data.nosync/test.npy', shuffle=False)
        with open('data.nosync/lof_model.pickle', 'rb') as f:
            classifier = pickle.load(f)


        prs = []
        prec_rec = []
        data_normal = dev.get_all_normal()[:10000]
        anom = dev.get_all_anomalous()
        for i in range(24):
            data_anom = anom[i*18:i*18 + 18]

            predicted_anom = - classifier.decision_function(data_anom)
            predicted_normal = - classifier.decision_function(data_normal)

            gold = np.concatenate((np.zeros(len(data_normal)), np.ones(len(data_anom)))) 
            pred = np.concatenate((sigmoid2(predicted_normal), sigmoid2(predicted_anom)))

            prec, rec, th, pr2 = compute_precision_recall(gold, pred) 
            prs.append(pr2)
            prec_rec.append((prec, rec))
        print(np.average(prs))
        np.savez('evals/lof_prec_rec.npz', prec_rec)
        np.save('evals/lof_prec_au.npy', prs)


        exit()




        gold = np.concatenate((np.zeros(len(data_normal)), np.ones(len(data_anom)))) 
        pred = np.concatenate((sigmoid2(predicted_normal), sigmoid2(predicted_anom)))

        prec, rec, th, pr2 = compute_precision_recall(gold, pred) 

        plt.rc('font', family='serif')
        f = plt.figure()
        plt.plot(rec, prec, color='black', label='Precision Recall curve (area = {:.3f})'.format(pr2))

        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.legend(loc="lower right")
        plt.title('Precision Recall curve of OC-SVM')

        f.savefig("foo_svm.pdf", bbox_inches='tight')

        plt.show()


        exit()
        cm = metrics.confusion_matrix(gold, pred)
        print("cm" + str(cm))

        prec = metrics.precision_score(gold, pred, pos_label=-1)
        print("prec " + str(prec))

        rec = metrics.recall_score(gold, pred, pos_label=-1)
        print("rec " + str(rec))
