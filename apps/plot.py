#!/usr/bin/env python3
import numpy as np
from sklearn import metrics
import matplotlib.pyplot as plt

# True = AUC, False = PR
metric = False
# data = np.load('evals/eval_test_2019-03-05_141120.npz') 
data = np.load('evals/eval_test_2019-03-09_132241.npz')  #best
# data = np.load('evals/eval_test_2019-03-05_162940.npz') 

if metric:
    auc = metrics.auc(data['fpr'], data['tpr'])
    print(auc)
    plt.rc('font', family='serif')
    f = plt.figure()

    plt.plot(data['fpr'], data['tpr'], color='black', label='ROC curve (area = {:.3f})'.format(auc))
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc="lower right")
    plt.title('ROC curve of the test set')

    f.savefig("foo.pdf", bbox_inches='tight')

    plt.show()
else:
    pr = data['pr']
    print(pr)
    print("recall {}".format(data['recall'])) 
    print(data['precision'])  
    print(len(data['recall'])) 
    print(len(data['precision'])) 

    plt.rc('font', family='serif')
    f = plt.figure()
    plt.plot(data['recall'], data['precision'], color='black', label='Precision Recall curve (area = {:.3f})'.format(pr))
    
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.legend(loc="lower right")
    plt.title('Precision Recall curve of GAN classifier')

    f.savefig("foo.pdf", bbox_inches='tight')

    plt.show()

