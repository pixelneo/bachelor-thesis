#!/usr/bin/env python3 
import numpy as np
import tensorflow as tf
import collections

from credit_card_dataset import CreditCardDataset, CrossValidator
from utils import * 

import matplotlib.pyplot as plt
'''
This file is based on gan.py template by Milan Straka https://github.com/ufal/npfl114/blob/master/labs/10/gan.py
However it has been extensively altered.

License: CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

def run_eval(encoder):
    normal_file = 'evals/mapping_test.npz-raw.npz' if not encoder else 'evals/encoder_test.npz-raw.npz'
    pr = 0
    with open(normal_file, 'rb') as f:
        data = np.load(f)
        prec_rec = []
        prs = []
        for i in range(26):
            y = np.zeros(len(data['normal_res']))
            y1 = np.ones(len(data['anom_res'][i*17:i*17 + 17]))
            y = np.concatenate((y, y1))
            scores = np.concatenate((np.array(data['normal_res']), np.array(data['anom_res'][i*17:i*17 + 17])))

            pr, prec, rec, _ = compute_precision_recall(y, scores)
            prs.append(pr)
            prec_rec.append((prec, rec))
        pr = np.array(prs)
        print('\nAverage area under PR curve: {}'.format(np.average(pr)))
    return pr


class Network:
    def __init__(self):
        graph = tf.Graph()
        graph.seed = 88
        self.dim = 28
        self.session = tf.Session(graph = graph, config=tf.ConfigProto(inter_op_parallelism_threads=1,
                                                                       intra_op_parallelism_threads=1))

    def construct(self, args):
        self.z_dim= args.z_dim

        with self.session.graph.as_default():

            # Inputs
            self.inputs = tf.placeholder(tf.float32, [None, self.dim])
            self.z_input = tf.placeholder(tf.float32, [None, self.z_dim])
            self.is_trainingG = tf.placeholder(tf.bool, None)
            self.is_trainingD = tf.placeholder(tf.bool, None)
            self.g_lr = tf.placeholder(tf.float32, None)
            self.e_lr = tf.placeholder(tf.float32, None)
            self.d_lr = tf.placeholder(tf.float32, None)
            self.m_lr = tf.placeholder(tf.float32, None)

            # TODO how to create Variable from placeholder
            with tf.variable_scope('z_mapped'):
                self.z = tf.get_variable('z', shape=[1, args.z_dim], dtype=tf.float32, initializer=tf.initializers.random_normal(0,1))
            with tf.variable_scope('z_mapped', reuse=True):
                self.reset_z = tf.variables_initializer([tf.get_variable('z')])

            # Encoder 
            def encoder(data):
                hidden_layer = tf.layers.dense(data, 64)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)

                hidden_layer = tf.layers.dense(hidden_layer, 32)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                result = tf.layers.dense(hidden_layer, self.z_dim)
                return result

            with tf.variable_scope('encoder'):
                self.encoded = encoder(self.inputs)


            # Generator
            def generator(z):
                hidden_layer = tf.layers.dense(z, 32)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                hidden_layer = tf.layers.dropout(hidden_layer, args.dropout-0.1, training=self.is_trainingG) 

                hidden_layer = tf.layers.dense(z, 48)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                hidden_layer = tf.layers.dropout(hidden_layer, args.dropout-0.1, training=self.is_trainingG) 

                hidden_layer = tf.layers.dense(hidden_layer, 64)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                hidden_layer = tf.layers.dropout(hidden_layer, args.dropout-0.1, training=self.is_trainingG) 

                result = tf.layers.dense(hidden_layer, self.dim)
                return result

            with tf.variable_scope('generator'):
                self.generated = generator(self.z_input)

            with tf.variable_scope('generator', reuse= True):
                self.generated_map = generator(self.z)

            with tf.variable_scope('generator', reuse=True):
                self.encoded_generated = generator(self.encoded)

            # Critic
            def critic(data):
                hidden_layer = tf.layers.dense(data,64)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                hidden_layer = tf.layers.dropout(hidden_layer, args.dropout, training=self.is_trainingD)

                hidden_layer = tf.layers.dense(data,54)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                hidden_layer = tf.layers.dropout(hidden_layer, args.dropout, training=self.is_trainingD)

                hidden_layer = tf.layers.dense(hidden_layer,48)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                hidden_layer = tf.layers.dropout(hidden_layer, args.dropout, training=self.is_trainingD)

                hidden_layer = tf.layers.dense(hidden_layer,32)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                hidden_layer = tf.layers.dropout(hidden_layer, args.dropout, training=self.is_trainingD)

                intermediate_layer = tf.layers.dense(hidden_layer, 8)
                logit = tf.layers.dense(tf.nn.leaky_relu(intermediate_layer), 1)
                return logit[:, 0], intermediate_layer


            with tf.variable_scope('critic'):
                critic_logit_real, features_real = critic(self.inputs)

            with tf.variable_scope('critic', reuse = True):
                critic_logit_fake, features_fake  = critic(self.generated)

            with tf.variable_scope('critic', reuse = True):
                _, features_fake = critic(self.generated_map)

            self._d = critic_logit_fake

            # Losses
            self.critic_loss = - tf.reduce_mean(critic_logit_real) + tf.reduce_mean(critic_logit_fake)

            self.generator_loss = - tf.reduce_mean(critic_logit_fake)

            # Anomaly score loss
            self.residual_loss = tf.losses.absolute_difference(self.inputs, self.generated_map)
            self.discriminator_mapping_loss = tf.losses.absolute_difference(features_real, features_fake)
            self.anomaly_score = self.residual_loss

            # Encoder loss
            self.encoder_loss = tf.losses.mean_squared_error(self.inputs, self.encoded_generated)
            # Anomaly score using encoder
            self.anomaly_score_encoder = tf.losses.absolute_difference(self.inputs, self.encoded_generated)

            # Gradient penalty
            epsilon  = tf.random_uniform(shape=[args.batch_size, 1, 1, 1], minval=0., maxval=1.)
            x_hat = epsilon * self.inputs + (1 - epsilon) * self.generated
            with tf.variable_scope('critic', reuse = True):
                penalty_critic, _ = critic(x_hat)
            penalty_critic_gradients = tf.gradients(penalty_critic, [x_hat])[0]
            l2_norm = tf.sqrt(tf.reduce_sum(tf.square(penalty_critic_gradients)))
            penalty = tf.reduce_mean(tf.square(l2_norm - 1.))
            self.critic_loss = self.critic_loss + args.penal_coeff * penalty

            # Training
            self.global_step = tf.train.create_global_step()

            # Saving
            self.saver = tf.train.Saver(max_to_keep=5)


            self.critic_training = tf.train.AdamOptimizer(self.d_lr, beta1=0.1, beta2=0.9).minimize(
                self.critic_loss,
                var_list=tf.global_variables('critic'))

            self.generator_training = tf.train.AdamOptimizer(self.g_lr, beta1=0.1, beta2=0.9).minimize(
                self.generator_loss,
                global_step=self.global_step,
                var_list=tf.global_variables('generator'))

            self.mapping_training = tf.train.AdamOptimizer(self.m_lr).minimize(
                self.anomaly_score,
                global_step = self.global_step,
                var_list=tf.contrib.framework.get_variables_by_name('z')) # Optimize only coeffitients of `z`

            self.encoder_training = tf.train.AdamOptimizer(self.e_lr).minimize(
                self.anomaly_score_encoder,
                global_step = self.global_step,
                var_list=tf.global_variables('encoder'))

            # Summaries
            critic_accuracy = tf.reduce_mean(tf.to_float(tf.concat([tf.greater(critic_logit_real, 0), tf.less(critic_logit_fake, 0)], axis=0)))
            self.d_a = tf.reduce_mean(tf.to_float(tf.greater(critic_logit_real, 0)))
            self.d_b = tf.reduce_mean(tf.to_float(tf.less(critic_logit_fake, 0)))

            summary_writer = tf.contrib.summary.create_file_writer(args.logdir, flush_millis=10 * 1000)
            with summary_writer.as_default(), tf.contrib.summary.record_summaries_every_n_global_steps(50):
                self.critic_summary = [tf.contrib.summary.scalar('gan/discriminator_loss', self.critic_loss),
                                              tf.contrib.summary.scalar('gan/discriminator_accuracy', critic_accuracy)]
                self.generator_summary = tf.contrib.summary.scalar('gan/generator_loss', self.generator_loss)
                self.encoder_summary = [tf.contrib.summary.scalar('gan/encoder_loss', self.encoder_loss),
                                        tf.contrib.summary.scalar('gan/anomaly_score_encoder', self.anomaly_score_encoder)]

            summary_writer_mapping = tf.contrib.summary.create_file_writer(args.logdir, flush_millis=1000)
            with summary_writer_mapping.as_default(), tf.contrib.summary.record_summaries_every_n_global_steps(10):
                residual_summary = tf.contrib.summary.scalar('mapping/residual_loss', self.residual_loss)
                disc_map_summary = tf.contrib.summary.scalar('mapping/discriminator_mapping_loss', self.discriminator_mapping_loss)
                anomaly_score_summary = tf.contrib.summary.scalar('mapping/anomaly_score', self.anomaly_score)
                self.mapping_summary = tf.group(residual_summary, disc_map_summary, anomaly_score_summary)

            # Initialize variables
            self.session.run(tf.global_variables_initializer())

            with summary_writer.as_default():
                tf.contrib.summary.initialize(session=self.session, graph=self.session.graph)

    def sample_z(self, batch_size):
        return np.random.standard_normal([batch_size, self.z_dim])

    def train_critic(self, data, d_lr):
        loss = self.session.run([self.critic_training, self.critic_summary, self.critic_loss], {self.inputs: data, self.z_input: self.sample_z(len(data)), self.is_trainingD:True, self.is_trainingG:False, self.d_lr:d_lr})[-1]
        return loss

    def train_generator(self, batch_size, g_lr):
        loss = self.session.run([self.generator_training, self.generator_summary, self.generator_loss], {self.z_input: self.sample_z(batch_size), self.is_trainingG:True, self.is_trainingD:False, self.g_lr: g_lr})[-1]
        return loss

    def train_encoder(self, data, e_lr):
        loss = self.session.run([self.encoder_training, self.encoder_summary, self.anomaly_score_encoder], {self.inputs: data, self.is_trainingG:False, self.is_trainingD:False, self.e_lr: e_lr})[-1]
        return loss

    def get_anomaly_score_encoder(self, data):
        a_s = self.session.run([self.anomaly_score_encoder], {self.inputs: [data], self.is_trainingD: False, self.is_trainingG: False})[0]
        return a_s

    def get_latent(self, data):
        a_s, enc= self.session.run([self.anomaly_score_encoder, self.encoded], {self.inputs: [data], self.is_trainingD: False, self.is_trainingG: False})
        return a_s, enc


    def train_anomaly_score(self, data, iterations, map_lr):
        self.session.run(self.reset_z, {self.is_trainingG: False, self.is_trainingD: False})
        x = 0
        m_lr = map_lr
        for i in range(iterations):
            if i == iterations - 100:
                m_lr = map_lr / 2
            x = self.session.run([self.anomaly_score, self.z, self.generated_map, self.mapping_training, self.mapping_summary], {self.inputs: [data], self.is_trainingD:False, self.is_trainingG:False, self.m_lr:m_lr})[:3]
        return x

    def save(self, path):
        self.saver.save(self.session, path, global_step = self.global_step)

    def restore(self, path):
        self.saver.restore(self.session, path)

    def restore_last(self, path):
        chk = tf.train.latest_checkpoint(path)
        self.saver.restore(self.session, chk)

def train(network, args, data=None):
    ''' Trains the network '''
    print('--- Training ---')
    # Load dataset
    train = CreditCardDataset.from_file('./data.nosync/train.npy') if not data else CreditCardDataset.from_file(data)
    d_lr = args.d_lr
    g_lr = args.g_lr
    gen_loss = 0
    gen_loss_sum = 0
    start = 0
    k = 0
    changed = [False]*args.epochs 
    c_increase = 0

    if args.restore:
        network.restore(args.model_restore)
        k = 94500
        start = 10
        d_lr = d_lr/(2*(1))
        g_lr = g_lr/(2*(1))
        print('restored')
    else:
        # Initial critic training
        for _ in range(args.c_start*2):
            data = train.next_batch(args.batch_size)
            crit_loss = network.train_critic(data, args.d_lr)

    # Train
    for i in range(start, args.epochs):
        # adjust lr
        if not changed[i] and i % 2 == 0 and i > 0 and g_lr > 5.0e-7 and d_lr > 0.59e-8:
            # c_increase -= 
            d_lr = d_lr/1.25
            g_lr = g_lr/(1.65)
            changed[i] = True
            # print('changed dkr: {}, g_lr: {}'.format(d_lr, g_lr))

        # Initial critic training
        for _ in range(args.c_start):
            data = train.next_batch(args.batch_size)
            crit_loss = network.train_critic(data, d_lr)
        # print('Critic pretrained on {} in epoch {}'.format(args.c_start, i))

        if i > 0:
            # Initial generator training
            for _ in range(args.g_start):
                gen_loss = network.train_generator(args.batch_size, g_lr)

        loss = 0
        while not train.epoch_finished:
            k +=1
            for _ in range(args.c_iter + args.c_increase * int(i/1)):
                data = train.next_batch(args.batch_size)
                crit_loss = network.train_critic(data, d_lr)
                loss += crit_loss

            # Train generator args.g_iter iterations
            for _ in range(args.g_iter):
                gen_loss = network.train_generator(args.batch_size, g_lr)
                loss+= gen_loss
                gen_loss_sum += gen_loss/(500*args.g_iter)

            if k % 5000 == 0:
                network.save(args.model_save)

            # Print some stats
            if k % 500 == 0:
                # print('Iter: {}, epo {}'.format(k,i))
                # print('item: {}     Loss {:.2f}    Gen Loss {:.2f}     Gen AVG Loss {:.2f}'.format(k * args.c_iter * args.batch_size, loss, gen_loss, gen_loss_sum))
                # print('')
                gen_loss_sum = 0

        print('\n---------\nNew Epoch ({}) '.format(i+1))

    # print('finish generator')
    for _ in range(10):
        gen_loss = network.train_generator(args.batch_size, g_lr)

    # final save
    network.save(args.model_save)
    print('saved ')




def evaluate(network, args, test_file=None, data=None, shuffle=False):
    dev = CreditCardDataset.from_file(test_file, shuffle=shuffle) if not data else data
    anom_res, normal_res = [], []

    anom = dev.get_anomalous_samples(min(dev.anomalous_size, args.anom_cases))
    normal = dev.get_normal_samples(min(dev.normal_size, args.norm_cases)) 
    cnt = 0
    for n in normal:
        ascore, z, gen = network.train_anomaly_score(n, args.map_iter, args.m_lr)
        # print(ascore)
        normal_res.append(ascore)
        # if cnt % 100 == 0:
            # print(cnt)
        cnt+= 1

    # print('\n\n--------- ANOM ------ \n')
    for a in anom:
        ascore, z, gen = network.train_anomaly_score(a, args.map_iter, args.m_lr)
        # print(ascore)
        anom_res.append(ascore)

    y = np.zeros(len(normal_res))
    y1 = np.ones(len(anom_res))
    y = np.concatenate((y, y1))
    scores = np.concatenate((np.array(normal_res), np.array(anom_res)))

    np.savez('{}-raw'.format(args.eval_file), normal_res=normal_res, anom_res=anom_res)

    return (normal_res, anom_res)

def finish(netowrk, args):
    if args.restore:
        network.restore(args.model_restore)
        print('restored')
    elif args.restore_last:
        network.restore_last(args.model_restore)
        print('restored')
    ds = CreditCardDataset.from_file('./data.nosync/train.npy', shuffle=True)
    lr = args.d_lr
    for i in range(2400):
        data = ds.next_batch(args.batch_size)
        crit_loss = network.train_critic(data, lr)
        if i == 2000:
            lr = args.d_lr / 3

    print('FInish - Critic pretrained on {}'.format(args.c_start))
    network.save('./data.nosync/model/finished_{}'.format('bla_better.ckpt'))


def train_encoder(network, args, data=None):
    print('---Training encoder ---')
    train = CreditCardDataset.from_file('./data.nosync/train.npy') if not data else CreditCardDataset.from_file(data)
    if args.restore:
        network.restore(args.model_restore)
        print('restored')
    elif args.restore_last:
        network.restore_last(args.model_restore)
        print('restored')

    k = 0
    changed = [False]*args.epochs_encoder
    c_increase = 0
    e_lr = args.e_lr
    e_loss = 0
    e_loss_sum = 0
    # Train
    for i in range(args.epochs_encoder):
        # adjust lr
        if not changed[i] and i % 4 == 0 and i > 0: 
            e_lr = e_lr/2
            changed[i] = True
            print('changed lr: {}'.format(e_lr))

        loss = 0
        while not train.epoch_finished:
            k +=1
            data = train.next_batch(args.batch_size)
            e_loss = network.train_encoder(data, e_lr)
            loss += e_loss
            e_loss_sum += e_loss/(500)

            # Print some stats
            if k % 500 == 0:
                # print('Iter: {}, epo {}'.format(k,i))
                # print('item: {}     Enc Loss {:.2f}     Enc AVG Loss {:.2f}'.format(k * args.c_iter * args.batch_size, e_loss, e_loss_sum))
                # print('')
                e_loss_sum = 0


        # print('\n---------\nNew Epoch ({}), loss: {:.3f}'.format(i,loss))

    network.save(args.model_save)
    print('saved')

def evaluate_encoder(network, args, test_file=None, data=None, shuffle=False):
    # print('--- Evaluating ---')
    dev = CreditCardDataset.from_file(test_file, shuffle=shuffle) if not data else data
    print(dev.get_all_normal().shape)
    print(dev.get_all_anomalous().shape)
    # Evaluation
    anom_res, normal_res = [], []

    anom = dev.get_anomalous_samples(min(dev.anomalous_size, args.anom_cases))
    normal = dev.get_normal_samples(min(dev.normal_size, args.norm_cases)) 
    cnt = 0
    for n in normal:
        ascore = network.get_anomaly_score_encoder(n)
        # print(ascore)
        normal_res.append(ascore)
        # if cnt % 100 == 0:
            # print(cnt)
        cnt+= 1

    # print('\n\n--------- ANOM ------ \n')
    for a in anom:
        ascore = network.get_anomaly_score_encoder(a)
        # print(ascore)
        anom_res.append(ascore)

    y = np.zeros(len(normal_res))
    y1 = np.ones(len(anom_res))
    y = np.concatenate((y, y1))
    scores = np.concatenate((np.array(normal_res), np.array(anom_res)))

    np.savez('{}-raw'.format(args.eval_file), normal_res=normal_res, anom_res=anom_res)

    return (normal_res, anom_res)

def get_latent(network, args, data, file):
    encs = []
    for d in data:
        ascore, enc = network.get_latent(d)
        encs.append(enc[0])
    np.save(file, np.array(encs))

def run_eval2(normal_res, anom_res):
    pr = 0
    prec_rec = []
    prs = []
    for i in range(26):
        y = np.zeros(len(normal_res))
        y1 = np.ones(len(anom_res[i*17:i*17 + 17]))
        y = np.concatenate((y, y1))
        scores = np.concatenate((np.array(normal_res), np.array(anom_res[i*17:i*17 + 17])))

        pr, prec, rec, _ = compute_precision_recall(y, scores)
        print('run_eval2 pr: {:.5f}'.format(pr))
        prs.append(pr)
        prec_rec.append((prec, rec))
    pr = np.array(prs)
    return pr, prec_rec


if __name__ == '__main__':
    import argparse
    import datetime
    import os
    import re

    np.random.seed(88)

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--batch_size', default=4, type=int, help='Batch size.')
    parser.add_argument('--epochs', default=9, type=int, help='Number of epochs.')
    parser.add_argument('--epochs_encoder', default=5, type=int, help='Number of epochs for encoder training.')
    parser.add_argument('--z_dim', default=12, type=int, help='Dimensionality of latent space.')
    parser.add_argument('--dropout', default=0.3, type=float, help='Dropout rate.')
    parser.add_argument('--g_start', default=30, type=int, help='Generator iteration at the beginnig of epoch.')
    parser.add_argument('--c_start', default=1000, type=int, help='Critic iterations before starting to train generator.')
    parser.add_argument('--c_increase', default=1, type=int, help='Multiply epoch iter to increase critic training.')
    parser.add_argument('--c_iter', default=6, type=int, help='Critic iterations.')
    parser.add_argument('--g_iter', default=1, type=int, help='Generator iterations.')
    parser.add_argument('--map_iter', default=80, type=int, help='Iterations of optimizer for mapping to latent space.')
    parser.add_argument('--map_w', default=0.0, type=float, help='Weight of discriminator in mapping to latent space.')
    parser.add_argument('--penal_coeff', default=12.0, type=float, help='Penalty weight.')
    parser.add_argument('--e_lr', default=5.0e-5, type=float, help='Encoder learning rate.')
    parser.add_argument('--d_lr', default=1.0e-4, type=float, help='Critic learning rate.')
    parser.add_argument('--g_lr', default=2.0e-5, type=float, help='Generator learning rate.')
    parser.add_argument('--m_lr', default=0.01, type=float, help='Mapping to latent space learning rate.')
    parser.add_argument('--eval', action='store_true', help='Start evaluating without training.')
    parser.add_argument('--cross', action='store_true', help='Run cross validation.')
    parser.add_argument('--cross_encoder', action='store_true', help='Run cross validation on encoder.')
    parser.add_argument('--cross_fold', default=0, type=int, help='Which data to use to train and test.')
    parser.add_argument('--model_restore', default='data.nosync/model/mx39-adam.ckpt-94770', type=str, help='Path to model to restore.')
    parser.add_argument('--restore', action='store_true', help='Restore before training.')
    parser.add_argument('--restore_last', action='store_true', help='Restore before training from last checkpoint.')
    parser.add_argument('--devel', default=True, type=bool, help='Use devel set. Test set otherwise.')
    parser.add_argument('--model_save', default='data.nosync/model/mdBN.ckpt', type=str, help='Path to model to save.')
    parser.add_argument('--norm_cases', default=10e9, type=int, help='How many samples use for eval.')
    parser.add_argument('--anom_cases', default=500, type=int, help='How many samples use for eval.')
    parser.add_argument('--eval_file', default=None, type=str, help='File to which metrics will be saved.')
    parser.add_argument('--finish', action='store_true', help='Finish by training critic.')
    parser.add_argument('--train_encoder', action='store_true', help='Train encoder.')


    args = parser.parse_args()

    # Create logdir name
    args.logdir = 'logs/{}-{}-{}'.format(
        os.path.basename(__file__),
        datetime.datetime.now().strftime('%Y-%m-%d_%H%M%S'),
        ','.join(('{}={}'.format(re.sub('(.)[^_]*_?', r'\1', key), value) for key, value in sorted(vars(args).items())))
    )
    if not os.path.exists('logs'): os.mkdir('logs')


    # Construct the network
    network = Network()
    network.construct(args)
    if args.restore:
        network.restore(args.model_restore)
    elif args.restore_last:
        network.restore_last(args.model_restore)

    if args.finish:
        finish(network, args)
    elif args.train_encoder:
        train_encoder(network, args)
    elif args.eval:
        print('--eval--')
        evaluate(network, args, 'data.nosync/test0.npy')
    elif args.cross:
        train_d = './data.nosync/train{}.npy'.format(args.cross_fold)
        test_d = './data.nosync/test{}.npy'.format(args.cross_fold)
        prs = []
        args.eval_file = 'evals/mapping_test{}.npz'.format(args.cross_fold)
        train(network, args, train_d)
        n,a = evaluate(network, args, test_d)
        pr, prec_rec = run_eval2(n, a)
        np.savez('evals/prec_rec{}.npz'.format(args.cross_fold), prec_rec)
        pr2 = np.average(pr)
        print('AUPRC {:.6f}'.format(pr2))
        np.savez('evals/prec_au.npy', pr2)
    elif args.cross_encoder:
        train_d = './data.nosync/train{}.npy'.format(args.cross_fold)
        test_d = './data.nosync/test{}.npy'.format(args.cross_fold)
        prs = []
        args.eval_file = 'evals/encoder_test.npz'.format(args.cross_fold)
        train(network, args, train_d)
        args.batch_size=32
        train_encoder(network, args, train_d)
        n, a = evaluate_encoder(network, args, test_d)
        pr, prec_rec = run_eval2(n, a)
        np.savez('evals/prec_rec_encoder{}.npz'.format(args.cross_fold), prec_rec)
        pr2 = np.average(pr)
        print('{:.6f}'.format(pr2))
        np.savez('evals/prec_au_encoder.npy', pr2)
    elif not args.eval:
        train(network, args)



