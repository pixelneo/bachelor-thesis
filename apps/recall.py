#!/usr/bin/env python3
import numpy as np
from utils import *

import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt

def get_prec(file2,l):
    res = []
    for f in range(29):
        file = np.load('evals.nosync/{}{}.npz'.format(file2,f))
        dt = file['arr_0']
        res_0 = []
        for i, (prec, rec) in enumerate(dt):
            recall0 = np.flip(rec)
            precision0 = np.flip(prec)
            index = np.where(recall0 >=l - 0.001)[0][0]
            # print('\nprec, rec, ')
            # print(precision0[index])
            # print(recall0[index])
            res_0.append(precision0[index])
            if recall0[index] < l:
                print("WARNING, {}, {}, {}".format(f,i,recall0[index]))
        avg = (np.average(res_0))
        # print(avg)
        res.append(avg)
    return res

def get_prec2(raw,file2,lvl):
    res = []
    for f in range(29):
        dt = raw[file2][f]
        res_0 = []
        for i, (precision0, recall0) in enumerate(dt):
            index = np.where(recall0 >=lvl)[0][0]
            assert len(precision0) == len(recall0)
            res_0.append(precision0[index])
        avg = np.average(res_0)
        res.append(avg)
    return res

def get_prec_ebay(rec, prec,lvl):
    index = np.where(rec>=lvl)[0][0]
    res = prec[index]
    return res
 
def get_data():
    methods = ['prec_rec_encoder', 'svm_prec_rec', 'if_prec_rec', 'prec_rec']
    data = {}
    for m in methods:
        temp = []
        for f in range(29):
            temp2 = []
            file = np.load('evals.nosync/{}{}.npz'.format(m,f))
            dt = file['arr_0']
            res_0 = []
            for i, (prec, rec) in enumerate(dt):
                recall0 = np.flip(rec)
                precision0 = np.flip(prec)
                temp2.append((precision0, recall0))
            temp.append(temp2)
        data[m] = temp
    return data


def get_diff():
    levels = np.arange(0.1,1.1,0.1)
    methods = ['prec_rec_encoder', 'svm_prec_rec', 'if_prec_rec', 'prec_rec']
    ress2 = []
    scores = dict([(m,[]) for m in methods])
    scores['ebay'] = []

    ebay_rec = np.array([0.0,0.01977,0.02004,0.02101,0.02966,0.04573,0.06551,0.10012,0.10580,0.11952,0.15267,0.15725,0.16525,0.16868,0.20755,0.21441,0.23042,0.24871,0.25672,0.26415,0.27615,0.28301,0.30016,0.33789,0.34818,0.36167,0.37677,0.38706,0.40558,0.41793,0.43302,0.44994,0.47166,0.49247,0.50139,0.51740,0.56770,0.59629,0.62144,0.65002,0.68394,0.70604,0.71900,0.73463,0.74492,0.76321,0.78608,0.80208,0.82609,0.88504,0.99753])
    ebay_prec = np.array([1.0,0.99530,0.59488,0.80609,0.67918,0.75471,0.81828,0.79329,0.73114,0.73785,0.77290,0.66160,0.63505,0.56805,0.60811,0.45860,0.46643,0.37403,0.33474,0.28991,0.24068,0.21634,0.20867,0.21217,0.17788,0.15712,0.13349,0.12379,0.10350,0.09779,0.08235,0.07092,0.06327,0.05871,0.05122,0.05130,0.05376,0.05389,0.05401,0.05193,0.04767,0.04260,0.03306,0.02243,0.01990,0.01924,0.01492,0.01112,0.00847,0.00783,0.00477])
    for l in levels:
        # print('\nLevel {}'.format(l))
        ress = []
        print(l)
        for m in methods:
            # print(m)
            res = get_prec(m,l)
            res_avg = np.average(res)
            scores[m].append(res_avg)
            ress.append(res_avg)
            # print('prec: {:.4f}'.format(res_avg))
            # print('1-prec: {:.6f}'.format(1-res_avg))
            # print('')
        # ebay
        res = get_prec_ebay(ebay_rec, ebay_prec, l)

        # print('EBAY')
        # print('prec: {:.4f}'.format(res))
        scores['ebay'].append(res)
        # print('1-prec: {:.4f}'.format(1-res))
        # print('')
        # print('\nDiff precision ano - svm = {:.6f}'.format(np.diff(np.flip(ress))[0]))
        # print('\nDiff precision ano - EBAY= {:.6f}'.format(ress[0] - res))
        # print('\nRatio 1-prec ano/svm= {:.6f}'.format((1-ress[1])/(1-ress[0])))
        # print('\nRatio 1-prec ano/EBAY= {:.6f}'.format((1-ress[1])/(1-res)))
        # print('----------')
    for s in scores.keys():
        print(s)
        print(' & '.join(['{:.2f}'.format(x) for x in scores[s]]))
        print('')

def get_curve(raw):
    plt.rc('font', family='serif')
    # fig = plt.figure(figsize=(7,5))
    fig, ax1 = plt.subplots()

    ax1.set_xlabel('Recall')
    ax1.set_ylabel('Precision')
    ax1.tick_params(axis='y')

    levels = np.arange(0.0,1.02,0.02)
    methods = ['prec_rec_encoder', 'svm_prec_rec','if_prec_rec', 'prec_rec']
    names = ['AnoWGAN+e', 'OC-SVM','Isolation Forest', 'AnoWGAN–e']
    scores = [0.4625, 0.4113, 0.1827, 0.2706]
    colours = ['navy', 'green', 'fuchsia', 'red']
    types = ['-','--', '-.',':']

    for m,n,s,c,t in zip(methods, names, scores,colours, types):
        ress = []
        for l in levels:
            res = get_prec2(raw,m,l)
            res_avg = np.average(res)
            ress.append(res_avg)
        ress = np.array(ress)
        poly = np.polyfit(levels, ress, 15)
        y = np.polyval(poly, levels)
        y = np.clip(y, 0,1)
        ax1.plot(levels, y,color=c, linestyle=t, label='{} (auprc = {:.4f})'.format(n,s))
        # ax1.plot(levels, ress,color=c, linestyle=t, label='{} (auprc = {:.4f})'.format(n,s))

    # Add eBay
    ebay_rec = np.array([0.0,0.01977,0.02004,0.02101,0.02966,0.04573,0.06551,0.10012,0.10580,0.11952,0.15267,0.15725,0.16525,0.16868,0.20755,0.21441,0.23042,0.24871,0.25672,0.26415,0.27615,0.28301,0.30016,0.33789,0.34818,0.36167,0.37677,0.38706,0.40558,0.41793,0.43302,0.44994,0.47166,0.49247,0.50139,0.51740,0.56770,0.59629,0.62144,0.65002,0.68394,0.70604,0.71900,0.73463,0.74492,0.76321,0.78608,0.80208,0.82609,0.88504,0.99753])
    ebay_prec = np.array([1.0,0.99530,0.59488,0.80609,0.67918,0.75471,0.81828,0.79329,0.73114,0.73785,0.77290,0.66160,0.63505,0.56805,0.60811,0.45860,0.46643,0.37403,0.33474,0.28991,0.24068,0.21634,0.20867,0.21217,0.17788,0.15712,0.13349,0.12379,0.10350,0.09779,0.08235,0.07092,0.06327,0.05871,0.05122,0.05130,0.05376,0.05389,0.05401,0.05193,0.04767,0.04260,0.03306,0.02243,0.01990,0.01924,0.01492,0.01112,0.00847,0.00783,0.00477])
    # for l in levels:
        # res = get_prec_ebay(ebay_rec, ebay_prec,l)
    poly = np.polyfit(ebay_rec, ebay_prec, 15)
    y = np.polyval(poly, ebay_rec)
    y = np.clip(y, 0,1)
    # ax1.plot(ebay_rec, ebay_prec,color='yellow', linestyle='-', label='eBay cluster (auprc = {:.4f})'.format(0.2231))
    ax1.plot(ebay_rec, y,color='gold', linestyle='--', label='K-Means ensemble (auprc = {:.4f})'.format(0.2231))



    # Add FDR axis
    # plt.axis((-0.0,1.0,-0.0,1.0))
    # ax2 = ax1.twinx()
    # ax2.set_ylabel('False discovery rate')
    # ax2.set_yticklabels(['1.0','1.0','0.8','0.6','0.4','0.2','0.0'])
    # ax2.axis((-0.03,1.03,-0.03,1.03))
    ax1.axis((-0.03,1.03,-0.03,1.03))

    # ax2.plot(levels, y, linestyle='')

    # plt.xlabel('Recall')
    # plt.ylabel('Precision')
    ax1.legend(loc="upper right")
    plt.title('Averaged PR-curves')

    fig.savefig("test.pdf", bbox_inches='tight')



if __name__ == '__main__':
    data = get_data()
    get_curve(data)
    # get_diff()
    print('end')


