#!/usr/bin/env python3

import numpy as np
from sklearn import svm, metrics
from credit_card_dataset import CreditCardDataset, CrossValidator

import pickle
import argparse

def sigmoid2(x):
    # return x
    return 1 / (1 + np.exp(-x))

def compute_precision_recall(y, scores, file=None):
    precision, recall, thresholds = metrics.precision_recall_curve(y, scores, pos_label=1)
    pr = metrics.average_precision_score(y, scores, pos_label=1)
    if file:
        np.savez(file, precision=precision, recall=recall, thresholds=thresholds, pr=pr)
        return pr
    else:
        return (precision, recall, thresholds, pr)

def run_eval2(classifier, dev):
    prs = []
    prec_rec = []
    anom = dev.get_all_anomalous()
    data_normal = dev.get_all_normal()
    predicted_normal = - classifier.decision_function(data_normal)
    for i in range(26):
        data_anom = anom[i*17:i*17 + 17]
        predicted_anom = - classifier.decision_function(data_anom)

        gold = np.concatenate((np.zeros(len(data_normal)), np.ones(len(data_anom)))) 
        pred = np.concatenate((sigmoid2(predicted_normal), sigmoid2(predicted_anom)))

        prec, rec, th, pr2 = compute_precision_recall(gold, pred) 
        prs.append(pr2)
        prec_rec.append((prec, rec))
    print(np.average(prs))
    return np.array(prs), prec_rec


if __name__ == '__main__':
    np.random.seed(41)
    np.set_printoptions(threshold=np.inf)
    parser = argparse.ArgumentParser()
    parser.add_argument("--train", action='store_true', help="Train the classifier.")
    parser.add_argument("--cross", action='store_true', help="Cross validate.")
    # parser.add_argument("--test", action='store_true', help="Test the classifier.")
    args = parser.parse_args()
    
    classifier = svm.OneClassSVM(coef0=0.0, degree=3, gamma=0.007, kernel='rbf', max_iter=-1, nu=0.0005, random_state=41, shrinking=True, tol=0.001, cache_size=4000)
    if args.train:
        train = CreditCardDataset.from_file('data.nosync/train.npy')

        data  = train.get_all_normal()#[:0000]
        classifier.fit(data)
        with open('data.nosync/svm_model.pickle', 'wb') as f:
            pickle.dump(classifier, f)

    elif args.cross:
        prs = []
        print("start")
        for i in range(24):
            train_d = CreditCardDataset.from_file('./data.nosync/train{}.npy'.format(i))
            test_d = CreditCardDataset.from_file('./data.nosync/test{}.npy'.format(i))
            classifier = svm.OneClassSVM(cache_size=8000, coef0=0.0, degree=3, gamma=0.007, kernel='rbf',max_iter=-1, nu=0.0005, random_state=41,shrinking=True, tol=0.001, verbose=False) 
            # classifier = svm.OneClassSVM(kernel='linear',gamma='auto', coef0=0.0, tol=0.001, nu=0.5, shrinking=True, verbose=False, max_iter=-1, cache_size=8000, random_state=41)
            # classifier = svm.OneClassSVM(coef0=0.0, degree=3, gamma=0.007, kernel='rbf', max_iter=-1, nu=0.0005, random_state=41, shrinking=True, tol=0.001, cache_size=4000)

            data = train_d.get_all_normal()
            classifier.fit(data)

            pr, prec_rec = run_eval2(classifier, test_d)
            
            np.savez('evals/svm_prec_rec{}.npz'.format(i), prec_rec) 
            prs.append(np.average(pr))
        print('\n\n\n-------- Result of crossvalidation --------- \n')
        print('Average AUPRC = {}'.format(np.average(np.array(prs))))
        print('Var = {}'.format(np.var(np.array(prs))))
        print('Std = {}'.format(np.std(np.array(prs))))

    else:
        dev = CreditCardDataset.from_file('data.nosync/test.npy', shuffle=False)
        with open('data.nosync/svm_model.pickle', 'rb') as f:
            classifier = pickle.load(f)

        prs = []
        prec_rec = []
        anom = dev.get_all_anomalous()
        data_normal = dev.get_all_normal()[:9735]
        predicted_normal = - classifier.decision_function(data_normal)
        for i in range(26):
            data_anom = anom[i*17:i*17 + 17]
            predicted_anom = - classifier.decision_function(data_anom)

            gold = np.concatenate((np.zeros(len(data_normal)), np.ones(len(data_anom)))) 
            pred = np.concatenate((sigmoid2(predicted_normal), sigmoid2(predicted_anom)))

            prec, rec, th, pr2 = compute_precision_recall(gold, pred) 
            prs.append(pr2)
            prec_rec.append((prec, rec))
        print(np.average(prs))
        np.savez('evals/svm_prec_rec.npz', prec_rec)
        np.save('evals/svm_prec_au.np', prs)


    exit()
