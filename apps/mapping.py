#!/usr/bin/env python3
import numpy as np
import tensorflow as tf
import collections

from credit_card_dataset import CreditCardDataset
from utils import * 

import matplotlib.pyplot as plt



from ad_wgan import *
# from ad_bigan import *



import os
import datetime
import re
import argparse
from sklearn.metrics import average_precision_score
if __name__ == '__main__':
    np.random.seed(42)
    

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--enc", action='store_true', help="Use devel set.")
    parser.add_argument("--test", action='store_true', help="Use devel set.")
    parser.add_argument("--adv", action='store_true', help="Use only one test.")
    args = parser.parse_args()
    args.norm_cases = 9735 
    args.anom_cases = 442
    args.model_restore = 'data.nosync/model/model.ckpt-86740'
    args.z_dim = 12 
    args.map_w = 0.0
    args.dropout = 0.3
    args.batch_size = 4 
    args.map_iter = 800 
    args.m_lr = 0.01 
    args.penal_coeff= 10 

    # Create logdir name
    args.logdir = "logs/{}-{}-{}".format(
        os.path.basename(__file__),
        datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S"),
        ",".join(("{}={}".format(re.sub("(.)[^_]*_?", r"\1", key), value) for key, value in sorted(vars(args).items())))
    )
    if not os.path.exists("logs"): os.mkdir("logs")


    # Construct the network
    network = Network()
    network.construct(args)
    network.restore(args.model_restore)


    test_file = './data.nosync/devel.npy' if not args.test else './data.nosync/test.npy'
    base_eval = './evals/eval_{}_{}'.format('test' if args.test else 'devel', datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S"))



    # test mapping
    if args.test:
        if args.enc:
            args.map_w = 0.0
            print('test')
            args.norm_cases = 10000
            args.anom_cases = 442
            args.eval_file = 'evals/encoder_test.npz'
            pr = evaluate_encoder(network, args, test_file, False)
            print(pr[0])


        elif not args.adv:
            args.map_w = 0.0
            print('test')
            args.norm_cases = 10000
            args.anom_cases = 442
            args.map_iter = 70
            args.m_lr = 0.09
            args.eval_file = 'evals/mapping_test.npz'
            pr = evaluate(network, args, test_file, False)
            print(pr[0])

        if args.adv:
            args.eval_file = 'discard.npz'
            normal_file = 'evals/mapping_test.npz-raw.npz'
            with open(normal_file, 'rb') as f:
                data = np.load(f)
                print(data.files)
                prec_rec = []
                prs = []
                for i in range(24):
                    y = np.zeros(len(data['normal_res']))
                    y1 = np.ones(len(data['anom_res'][i*18:i*18 + 18]))
                    y = np.concatenate((y, y1))
                    scores = np.concatenate((np.array(data['normal_res']), np.array(data['anom_res'][i*18:i*18 + 18])))

                    pr, prec, rec, _ = compute_precision_recall(y, scores, args.eval_file)
                    prs.append(pr)
                    prec_rec.append((prec, rec))
                pr = np.array(prs)
                print(np.average(pr))
                with open('evals/prec_rec.npz', 'wb') as f2:
                    np.savez(f2, prec_rec)
                with open('evals/prec_au.npy', 'wb') as f2:
                    np.save(f2, pr)

    else:               # development mapping
        if args.adv:    # hyper param search
            i = 0
            prs = []
            args.norm_cases = 1000
            args.anom_cases = 50# actually little less
            for m_lr in [0.01, 0.04, 0.05, 0.06, 0.09, 0.1]:
                for m_iter in [20, 70, 81, 90]:
                    network.restore(args.model_restore)
                    print("   Start i: {}".format(i))
                    args.eval_file = '{}_{}.npz'.format(base_eval,str(i))
                    args.map_iter = m_iter
                    args.m_lr = m_lr
                    pr = evaluate(network, args, test_file, False)
                    prs.append(pr)
                    print('{},      m_iter:{}      m_lr:{},   evalfile:{},  i:{}'.format(pr[0], m_iter, m_lr, args.eval_file, i))
                    with open('m_res.txt', 'w+') as f:
                        print('{},      m_iter:{}      m_lr:{},   evalfile:{},  i:{}'.format(pr[0], m_iter, m_lr, args.eval_file, i), file=f)

                    i += 1
        else:
            print('basic')
            test_file = './data.nosync/test.npy'
            args.norm_cases = 1
            args.anom_cases = 0       # actually little less
            args.eval_file = 'basic.npz'
            args.map_iter = 250
            args.m_lr = 0.012
            auc = evaluate(network, args, test_file, True)
            print('{},      m_iter:{}      m_lr:{},   evalfile:{}'.format(auc, args.map_iter, args.m_lr, args.eval_file))

