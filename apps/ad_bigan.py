#!/usr/bin/env python3 
import numpy as np
import tensorflow as tf
import collections

from credit_card_dataset import CreditCardDataset
from utils import * 

import matplotlib.pyplot as plt
"""
This file is based on gan.py template by Milan Straka https://github.com/ufal/npfl114/blob/master/labs/10/gan.py
However it has been extensively altered.

License: CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
"""

class Network:
    def __init__(self, threads):
        graph = tf.Graph()
        graph.seed = 88
        self.dim = 28
        self.session = tf.Session(graph = graph)#, config=tf.ConfigProto(inter_op_parallelism_threads=threads,
                                                                       #intra_op_parallelism_threads=threads))

    def construct(self, args):
        self.z_dim= args.z_dim

        with self.session.graph.as_default():

            # Inputs
            self.inputs = tf.placeholder(tf.float32, [None, self.dim])
            self.z_input = tf.placeholder(tf.float32, [None, self.z_dim])
            self.is_trainingG = tf.placeholder(tf.bool, None)
            self.is_trainingE = tf.placeholder(tf.bool, None)
            self.is_trainingD = tf.placeholder(tf.bool, None)
            self.g_lr = tf.placeholder(tf.float32, None)
            self.d_lr = tf.placeholder(tf.float32, None)
            self.e_lr = tf.placeholder(tf.float32, None)
            self.m_lr = tf.placeholder(tf.float32, None)


            # Generator
            def generator(z):
                hidden_layer = tf.layers.dense(z, 32)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                hidden_layer = tf.layers.dropout(hidden_layer, args.dropout, training=True) 

                hidden_layer = tf.layers.dense(hidden_layer, 64)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                hidden_layer = tf.layers.dropout(hidden_layer, args.dropout, training=True) 

                result = tf.layers.dense(hidden_layer, self.dim)
                return result

            with tf.variable_scope("generator"):
                self.generated = generator(self.z_input)


            # Encoder
            def encoder(x):
                hidden_layer = tf.layers.dense(x, 64)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                hidden_layer = tf.layers.dropout(hidden_layer, args.dropout, training=True) 

                hidden_layer = tf.layers.dense(hidden_layer, 32)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)
                hidden_layer = tf.layers.dropout(hidden_layer, args.dropout, training=True) 

                result = tf.layers.dense(hidden_layer, self.z_dim)
                return result

            with tf.variable_scope('encoder'):
                self.encoded = encoder(self.inputs)

            with tf.variable_scope("generator", reuse=True):
                self.generated_encoded = generator(self.encoded)

            # Critic foc x
            def critic_x(x):
                hidden_layer = tf.layers.dense(x,48)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)

                hidden_layer = tf.layers.dense(hidden_layer,32)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)

                hidden_layer = tf.layers.dense(hidden_layer,16)
                print(hidden_layer.shape)
                # hidden_layer = tf.reshape(hidden_layer, [args.batch_size,-1])
                return hidden_layer

            # Critic for z
            def critic_z(z):
                hidden_layer = tf.layers.dense(z,32)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)

                hidden_layer = tf.layers.dense(hidden_layer,16)
                return hidden_layer

            # Critic
            def critic(c_x, c_z):
                hidden_layer = tf.concat([c_x, c_z], axis=1)

                hidden_layer = tf.layers.dense(hidden_layer,16)
                hidden_layer = tf.nn.leaky_relu(hidden_layer)

                intermediate_layer = hidden_layer
                logit = tf.nn.sigmoid(tf.layers.dense(intermediate_layer, 1))
                return logit[:, 0], intermediate_layer




            with tf.variable_scope("critic"):
                critic_encoder, features_encoder = critic(critic_x(self.inputs), critic_z(self.encoded))

            with tf.variable_scope("critic", reuse = True):
                critic_generator, features_generator = critic(critic_x(self.generated), critic_z(self.z_input))
            
            def safe_log(x):
                return tf.log(x + 10e-8)


            # Losses
            self.critic_loss = -(tf.reduce_mean(safe_log(critic_encoder)) + tf.reduce_mean(1 - safe_log(critic_generator)))
            self.generator_loss = -(tf.reduce_mean(1 - safe_log(critic_encoder)) + tf.reduce_mean(safe_log(critic_generator)))
            self.encoder_loss = -(tf.reduce_mean(1 - safe_log(critic_encoder)) + tf.reduce_mean(safe_log(critic_generator)))

            # Anomaly score
            self.anomaly_score = tf.losses.absolute_difference(self.inputs, self.generated_encoded)

            # Training
            self.global_step = tf.train.create_global_step()

            # Saving
            self.saver = tf.train.Saver(max_to_keep=20)

            self.critic_training = tf.train.AdamOptimizer(self.d_lr, beta1=0.5, beta2=0.9).minimize(
                self.critic_loss,
                # global_step = self.global_step,
                var_list=tf.global_variables("critic"))

            self.encoder_training = tf.train.AdamOptimizer(self.e_lr, beta1 = 0.5, beta2=0.9).minimize(
                self.encoder_loss,
                var_list=tf.global_variables('encoder'))

            self.generator_training = tf.train.AdamOptimizer(self.g_lr, beta1 = 0.5, beta2=0.9).minimize(
                self.generator_loss,
                global_step=self.global_step,
                var_list=tf.global_variables("generator"))



            # Summaries
            critic_accuracy = tf.reduce_mean(tf.to_float(tf.concat([tf.greater(critic_encoder, 0), tf.less(critic_generator, 0)], axis=0)))

            summary_writer = tf.contrib.summary.create_file_writer(args.logdir, flush_millis=10 * 1000)
            with summary_writer.as_default(), tf.contrib.summary.record_summaries_every_n_global_steps(50):
                self.critic_summary = [tf.contrib.summary.scalar("gan/discriminator_loss", self.critic_loss),
                                              tf.contrib.summary.scalar("gan/discriminator_accuracy", critic_accuracy)]
                self.generator_summary = tf.contrib.summary.scalar("gan/generator_loss", self.generator_loss)
                self.encoder_summary = tf.contrib.summary.scalar("gan/encoder_loss", self.encoder_loss)


            # Initialize variables
            self.session.run(tf.global_variables_initializer())

            with summary_writer.as_default():
                tf.contrib.summary.initialize(session=self.session, graph=self.session.graph)

    def sample_z(self, batch_size):
        return np.random.standard_normal([batch_size, self.z_dim])

    def get_anomaly_score(self, data):
        score, ge = self.session.run([self.anomaly_score, self.generated_encoded], {self.inputs: [data], self.is_trainingD:False, self.is_trainingE:False, self.is_trainingG:False})
        return score, ge

    def train_critic(self, data, d_lr):
        loss = self.session.run([self.critic_training, self.critic_summary, self.critic_loss], {self.inputs: data, self.z_input: self.sample_z(len(data)), self.is_trainingE:False, self.is_trainingD:True, self.is_trainingG:False, self.d_lr:d_lr})[-1]
        return loss

    def train_generator(self, batch_size, data, g_lr, e_lr):
        loss = self.session.run([self.generator_training, self.generator_summary, self.generator_loss], {self.inputs:data, self.z_input: self.sample_z(len(data)), self.is_trainingE:False, self.is_trainingG:True, self.is_trainingD:False, self.g_lr: g_lr, self.e_lr:e_lr})[-1]
        loss2 = self.session.run([self.encoder_training, self.encoder_summary, self.encoder_loss], {self.inputs:data, self.z_input: self.sample_z(len(data)), self.is_trainingE:True, self.is_trainingG:False, self.is_trainingD:False, self.e_lr: e_lr, self.g_lr:g_lr})[-1]

        return loss, loss2

    def save(self, path):
        self.saver.save(self.session, path, global_step = self.global_step)

    def restore(self, path):
        print('restored')
        self.saver.restore(self.session, path)

def train(network, args, train_d=None):
    ''' Trains the network '''
    # Load dataset
    train = CreditCardDataset.from_file('./data.nosync/train.npy') if not train_d else CreditCardDataset.from_file(train_d)
    d_lr = args.d_lr
    g_lr = args.g_lr
    e_lr = args.e_lr
    gen_loss = 0
    enc_loss = 0
    gen_loss_sum = 0
    enc_loss_sum = 0
    start = 0
    k = 0
    changed = [False]*args.epochs 
    c_increase = 0
    # Train
    for i in range(start, args.epochs):
        print('new epoch')
        # adjust lr
        if not changed[i] and i % 2 == 0 and i > 0 and g_lr > 0.59e-8 and d_lr > 0.59e-8:
            # c_increase -= 
            d_lr = d_lr/2
            g_lr = g_lr/2
            e_lr = e_lr/2
            changed[i] = True
            print('changed dkr: {}, g_lr: {}'.format(d_lr, g_lr))

        # Initial critic training
        for _ in range(args.c_start):
            data = train.next_batch(args.batch_size)
            crit_loss = network.train_critic(data, d_lr)
        print('Critic pretrained on {} in epoch {}'.format(args.c_start, i))

        if i > 0:
            # Initial generator training
            for _ in range(args.g_start):
                data = train.next_batch(args.batch_size)
                gen_loss, enc_loss = network.train_generator(args.batch_size, data, g_lr, e_lr)
            print('Generator pretrained on {} in epoch {}'.format(args.c_start, i))

        loss = 0
        while not train.epoch_finished:
            k +=1
            for _ in range(args.c_iter + args.c_increase * int(i/2)):
                data = train.next_batch(args.batch_size)
                if len(data) < args.batch_size:
                    break
                # data = train.next_batch(args.batch_size)
                crit_loss = network.train_critic(data, d_lr)
                loss += crit_loss

            # Train generator args.g_iter iterations
            for _ in range(args.g_iter):
                data = train.next_batch(args.batch_size)
                if len(data) < args.batch_size:
                    break 
                gen_loss, enc_loss = network.train_generator(args.batch_size, data, g_lr, e_lr)
                loss+= gen_loss
                gen_loss_sum += gen_loss/(500*args.g_iter)
                enc_loss_sum += enc_loss/(500*args.g_iter)


            # Print some stats
            if k % 500 == 0:
                gen_loss_sum = 0
                enc_loss_sum = 0




def evaluate(network, args, test_file, shuffle=False):
    print('--evaluating--')
    dev = CreditCardDataset.from_file(test_file, shuffle=shuffle)
    # Evaluation
    anom_res, normal_res = [], []

    normal = dev.get_all_normal()
    anom = dev.get_all_anomalous()
    cnt = 0
    for n in normal:
        ascore, ge = network.get_anomaly_score(n)
        # print(n[:10])
        # print(ge[0][:10])
        normal_res.append(ascore)
        cnt+= 1

    for a in anom:
        ascore, ge = network.get_anomaly_score(a)
        # print(a[:10])
        # print(ge[0][:10])
        anom_res.append(ascore)

    y = np.zeros(len(normal_res))
    y1 = np.ones(len(anom_res))
    y = np.concatenate((y, y1))
    scores = np.concatenate((np.array(normal_res), np.array(anom_res)))

    np.savez('{}-raw'.format(args.eval_file), normal_res=normal_res, anom_res=anom_res)

    pr = compute_precision_recall(y, scores, args.eval_file)
    return (normal_res, anom_res)


def finish(netowrk, args):
    ds = CreditCardDataset.from_file('./data.nosync/train.npy', shuffle=True)
    lr = args.d_lr
    for i in range(2400):
        data = ds.next_batch(args.batch_size)
        crit_loss = network.train_critic(data, lr)

        if i == 2000:
            lr = args.d_lr / 3

    print('FInish - Critic pretrained on {}'.format(args.c_start))
    network.save('./data.nosync/model/finished_{}'.format('bla_better.ckpt'))



def run_eval2(normal_res, anom_res):
    pr = 0
    prec_rec = []
    prs = []
    for i in range(26):
        y = np.zeros(len(normal_res))
        y1 = np.ones(len(anom_res[i*17:i*17 + 17]))
        y = np.concatenate((y, y1))
        scores = np.concatenate((np.array(normal_res), np.array(anom_res[i*17:i*17 + 17])))

        pr, prec, rec, _ = compute_precision_recall(y, scores)
        print('run_eval2 pr: {:.5f}'.format(pr))
        prs.append(pr)
        prec_rec.append((prec, rec))
    pr = np.array(prs)
    return pr, prec_rec



if __name__ == "__main__":
    import argparse
    import datetime
    import os
    import re
    
    np.random.seed(88)
    
    # TODO change last layer size in critic to something smaller

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size", default=4, type=int, help="Batch size.")
    parser.add_argument("--epochs", default=15, type=int, help="Number of epochs.")
    parser.add_argument('--cross', action='store_true', help='Run cross validation.')
    parser.add_argument('--cross_fold', default=0, type=int, help='Which data to use to train and test.')
    parser.add_argument("--z_dim", default=12, type=int, help="Dimensionality of latent space.")
    parser.add_argument("--threads", default=4, type=int, help="Maximum number of threads to use.")
    parser.add_argument("--dropout", default=0.2, type=float, help="Dropout rate.")
    parser.add_argument("--g_start", default=0, type=int, help="Generator iteration at the beginnig of epoch.")
    parser.add_argument("--c_start", default=0, type=int, help="Critic iterations before starting to train generator.")
    parser.add_argument("--c_increase", default=0, type=int, help="Multiply epoch iter to increase critic training.")
    parser.add_argument("--c_iter", default=1, type=int, help="Critic iterations.")
    parser.add_argument("--g_iter", default=1, type=int, help="Generator iterations.")
    parser.add_argument("--map_iter", default=800, type=int, help="Iterations of optimizer for mapping to latent space.")
    parser.add_argument("--map_w", default=0.0, type=float, help="Weight of discriminator in mapping to latent space.")
    parser.add_argument("--penal_coeff", default=10.0, type=float, help="Penalty weight.")
    parser.add_argument("--d_lr", default=1.0e-4, type=float, help="Critic learning rate.")
    parser.add_argument("--e_lr", default=2.0e-5, type=float, help="Encoder learning rate.")
    parser.add_argument("--g_lr", default=2.0e-5, type=float, help="Generator learning rate.")
    parser.add_argument("--m_lr", default=0.01, type=float, help="Mapping to latent space learning rate.")
    parser.add_argument("--eval", default=False, type=bool, help="Start evaluating without training.")
    parser.add_argument("--model_restore", default='data.nosync/model/mxb_22.ckpt-190992', type=str, help="Path to model to restore.")
    parser.add_argument("--restore", default=False, type=bool, help="Restore before training.")
    parser.add_argument("--devel", default=True, type=bool, help="Use devel set. Test set otherwise.")
    parser.add_argument("--model_save", default='data.nosync/model/mdBN.ckpt', type=str, help="Path to model to save.")
    parser.add_argument("--norm_cases", default=30, type=int, help="How many samples use for eval.")
    parser.add_argument("--anom_cases", default=30, type=int, help="How many samples use for eval.")
    parser.add_argument("--eval_file", default=None, type=str, help="File to which metrics will be saved.")
    parser.add_argument("--finish", action='store_true', help="Finish by training critic.")


    args = parser.parse_args()

    # Create logdir name
    args.logdir = "logs/{}-{}-{}".format(
        os.path.basename(__file__),
        datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S"),
        ",".join(("{}={}".format(re.sub("(.)[^_]*_?", r"\1", key), value) for key, value in sorted(vars(args).items())))
    )
    if not os.path.exists("logs"): os.mkdir("logs")


    # Construct the network
    network = Network(threads=args.threads)
    network.construct(args)

    # --z_dim=12 --epochs=10 --c_increase=0 --dropout=0.3 --threads=8 --batch_size=12 --g_start=0 --c_start=0 --d_lr=1.0e-5 --g_lr=1.0e-5 --e_lr=1.0e-5 --c_iter=1 --g_iter=1 --model_save=data.nosync/model/mxBG-15.ckpt --model_restore=data.nosync/model/mx37-again.ckpt-53400 --cross --cross_fold=1 >
    if args.finish:
        finish(network, args)
    elif args.eval:
        network.restore(args.model_restore)
        evaluate(network, args, 'data.nosync/devel.npy')
    elif args.cross:
        train_d = './data.nosync/train{}.npy'.format(args.cross_fold)
        test_d = './data.nosync/test{}.npy'.format(args.cross_fold)
        prs = []
        args.eval_file = 'evals/bigan_test.npz'
        train(network, args, train_d)
        n,a = evaluate(network, args, test_d)
        pr, prec_rec = run_eval2(n, a)
        np.savez('evals/prec_rec_bigan{}.npz'.format(args.cross_fold), prec_rec)
        pr2 = np.average(pr)
        print('AUPRC {:.6f}'.format(pr2))
        np.savez('evals/prec_au_bigan.npy', pr2)
    elif not args.eval:
        train(network, args)
