#!/usr/bin/env python3
import numpy as np
from sklearn import metrics

import matplotlib as mpl
mpl.use('PDF')
import matplotlib.pyplot as plt

from scipy.signal import savgol_filter

data = np.loadtxt('data.nosync/scalars.csv', delimiter=',', dtype='f4', skiprows=1 )
data2 = np.loadtxt('data.nosync/scalars-2.csv', delimiter=',', dtype='f4', skiprows=1)
data3 = np.loadtxt('data.nosync/scalars-3.csv', delimiter=',', dtype='f4', skiprows=1)
data4 = np.loadtxt('data.nosync/scalars-4.csv', delimiter=',', dtype='f4', skiprows=1)
data5 = np.loadtxt('data.nosync/scalars-5.csv', delimiter=',', dtype='f4', skiprows=1)
data6 = np.loadtxt('data.nosync/scalars-6.csv', delimiter=',', dtype='f4', skiprows=1)
print(data.shape)
plt.rc('font', family='serif')
f = plt.figure(figsize=(7,4))

y = savgol_filter(data[:,2], 11, 3)
plt.plot(np.arange(1000),y, label='A: dicriminator_iter=1, batch_size=4', lw=0.8) # disc A ad_bigan.py-2019-04-15_092738-ac=30,batch_size=4,dicriminator_iter=0,dicriminator_iter=1,cs=0,dl=1e-05,d=True,d=0.3,el=1e-05,e=10,gi=1,gl=1e-05,gs=0,ml=0.01,nc=30,pc=10.0,zd=12

y = savgol_filter(data2[:,2], 11, 3)
plt.plot(np.arange(1000),y, label='B: dicriminator_iter=3, batch_size=4', lw=0.8)  # disc B ad_bigan.py-2019-04-15_095018-ac=30,batch_size=4,dicriminator_iter=0,dicriminator_iter=3,cs=0,dl=1e-05,d=True,d=0.3,el=1e-05,e=10,gi=1,gl=1e-05,gs=0,ml=0.01,nc=30,pc=10.0,zd=12

y = savgol_filter(data3[:,2], 11, 3)
plt.plot(np.arange(1000),y, label='C: dicriminator_iter=1, batch_size=12', lw=0.8) # disc C ad_bigan.py-2019-04-15_101044-ac=30,batch_size=12,dicriminator_iter=0,dicriminator_iter=1,cs=0,dl=1e-05,d=True,d=0.3,el=1e-05,e=10,e=False,ef=None,f=False,gi=1,gl=1e-05,gs=0,ml=0.01,nc=30,pc=10.0,r=False,t=8,zd=12

plt.xlabel('Time')
plt.ylabel('Loss')
plt.legend(loc="upper right")
plt.title('BiGAN discriminator loss')

f.savefig("foo2.pdf", bbox_inches='tight')


f = plt.figure(figsize=(7,4))
y = savgol_filter(data6[:,2], 11, 3)
plt.plot(np.arange(1000),y, label='A: dicriminator_iter=1, batch_size=4', lw=0.8) # gen A ad_bigan.py-2019-04-15_092738-ac=30,batch_size=4,dicriminator_iter=0,dicriminator_iter=1,cs=0,dl=1e-05,d=True,d=0.3,el=1e-05,e=10,gi=1,gl=1e-05,gs=0,ml=0.01,nc=30,pc=10.0,zd=12

y = savgol_filter(data5[:,2], 11, 3)
plt.plot(np.arange(1000),y, label='B: dicriminator_iter=3, batch_size=4', lw=0.8) # gen B ad_bigan.py-2019-04-15_095018-ac=30,batch_size=4,dicriminator_iter=0,dicriminator_iter=3,cs=0,dl=1e-05,d=True,d=0.3,el=1e-05,e=10,gi=1,gl=1e-05,gs=0,ml=0.01,nc=30,pc=10.0,zd=12

y = savgol_filter(data4[:,2], 11, 3)
plt.plot(np.arange(1000),y, label='C: dicriminator_iter=1, batch_size=12', lw=0.8) # gen C ad_bigan.py-2019-04-15_101044-ac=30,batch_size=12,dicriminator_iter=0,dicriminator_iter=1,cs=0,dl=1e-05,d=True,d=0.3,el=1e-05,e=10,e=False,ef=None,f=False,gi=1,gl=1e-05,gs=0,ml=0.01,nc=30,pc=10.0,r=False,t=8,zd=12



plt.xlabel('Time')
plt.ylabel('Loss')
plt.legend(loc="lower right")
plt.title('BiGAN generator loss')

f.savefig("foo.pdf", bbox_inches='tight')

plt.show()

