#!/usr/bin/env python3

import numpy as np
import pickle

FILE_NAME = 'data.nosync/creditcard.csv'
TEST_NORMAL_SIZE = 10000
DEV_NORMAL_SIZE = 2000
DEV_ANOM_SIZE = 50

data = np.loadtxt(FILE_NAME, skiprows=1,
                  delimiter = ',',
                  dtype = 'f4'
                 )

np.random.seed(42)
# np.random.shuffle(data)
# np.save('data.nosync/data',data)
# exit()

normal = data[np.where(data[:,30] == 0.)]
anomalous = data[np.where(data[:,30] == 1.)]


np.random.shuffle(normal)
np.random.shuffle(anomalous)

normal_size = len(normal)

training = normal[:normal_size - TEST_NORMAL_SIZE - DEV_NORMAL_SIZE]
test_normal = normal[(normal_size - TEST_NORMAL_SIZE - DEV_NORMAL_SIZE):(normal_size - DEV_NORMAL_SIZE)]

dev_normal = normal[(normal_size - DEV_NORMAL_SIZE):]

dev_anom = anomalous[:DEV_ANOM_SIZE]
test_anom = anomalous[DEV_ANOM_SIZE:]

testing = np.concatenate((test_normal, test_anom))
devel = np.concatenate((dev_normal, dev_anom))
np.random.shuffle(testing)
np.random.shuffle(devel)

# TODO: do i need class in training data?
np.save('data.nosync/test', testing)
np.save('data.nosync/devel', devel)
np.save('data.nosync/train', training)
