#!/usr/bin/env python3

import numpy as np
from sklearn.manifold import TSNE
from credit_card_dataset import CreditCardDataset

import pickle
import argparse
import matplotlib as mpl
mpl.use('PDF')

from matplotlib import pyplot as plt
from scipy.stats import gaussian_kde
from scipy.interpolate import interpn

def density_scatter( x , y, ax = None, sort = True, bins = 20, **kwargs )   :
    """
    Scatter plot colored by 2d histogram
    """
    if ax is None :
        fig , ax = plt.subplots()
        fig = plt.figure(figsize=(7,4.5))
    data , x_e, y_e = np.histogram2d( x, y, bins = bins)
    z = interpn( ( 0.5*(x_e[1:] + x_e[:-1]) , 0.5*(y_e[1:]+y_e[:-1]) ) , data , np.vstack([x,y]).T , method = "splinef2d", bounds_error = False )

    # Sort the points by density, so that the densest points are plotted last
    if sort :
        idx = z.argsort()
        x, y, z = x[idx], y[idx], z[idx]

    ax.scatter(x, y, c=z, s=20, **kwargs )
    plt.rc('image', cmap='PuRd')
    plt.title('Visualization of learned manifold')
    fig.savefig("foo_latent_10000.pdf", bbox_inches='tight')
    plt.show()
    return ax


if __name__ == '__main__':
    np.set_printoptions(threshold=np.inf)
    np.random.seed(41)

    parser = argparse.ArgumentParser()
    parser.add_argument("--normal", action='store_true', help="plot only onrmal latent space.")
    parser.add_argument("--all", action='store_true', help="plot data in latent space.")
    parser.add_argument("--real", action='store_true', help="plot real data space.")
    args = parser.parse_args()

    plt.rc('font', family='serif')
    plt.rc('image', cmap='PuRd')
    tsne = TSNE(n_components=2, perplexity=10)
    if args.all:
        fig = plt.figure()
        samples = 400
        normal_file = 'evals/latent_normal.npy'
        anom_file = 'evals/latent_anom.npy'
        with open(normal_file, 'rb') as f:
            with open(anom_file, 'rb') as f2:
                norm = np.load(f)
                anom = np.load(f2)

                conc = np.concatenate([norm[:,0], anom[:,0]])
                print(conc.shape)

                result = tsne.fit_transform(conc)
                n = result[:samples]
                a = result[samples:]

                # idx = z.argsort()
                # n = n[idx]

                plt.scatter(n[:,0], n[:,1], label='normal', edgecolor='', s=30)
                plt.scatter(a[:,0], a[:,1], label='anomalous',marker='H', edgecolor='', s=30)

                # plt.title('Visualization of learned manifold')
                plt.title('Representation of data points in latent space')
                plt.legend(loc="lower right")

                fig.savefig("foo_latent.pdf", bbox_inches='tight')
                plt.show()

    elif args.normal:
        normal_file = 'evals/latent_normal_10000.npy'
        with open(normal_file, 'rb') as f:
            norm = np.load(f)[:10000]
            result = tsne.fit_transform(norm)
            x = result[:,0]
            y = result[:,1]

            density_scatter(x,y, bins=20 )

            # plt.scatter(result[:,0], result[:,1], c=z, edgecolor='', s=30)
    elif args.real:
        fig = plt.figure()
        samples = 400
        normal_file = 'evals/data_normal.npy'
        anom_file = 'evals/data_anom.npy'
        with open(normal_file, 'rb') as f:
            with open(anom_file, 'rb') as f2:
                norm = np.load(f)
                anom = np.load(f2)

                conc = np.concatenate([norm, anom])
                print(conc.shape)

                result = tsne.fit_transform(conc)
                n = result[:samples]
                a = result[samples:]

                # idx = z.argsort()
                # n = n[idx]

                plt.scatter(n[:,0], n[:,1], label='normal', edgecolor='', s=30)
                plt.scatter(a[:,0], a[:,1], label='anomalous',marker='H', edgecolor='', s=30)

                # plt.title('Visualization of learned manifold')
                plt.title('Data points')
                plt.legend(loc="lower right")

                fig.savefig("foo_real.pdf", bbox_inches='tight')
                plt.show()
        
