#!/usr/bin/env python3
import numpy as np
from utils import *
from sklearn.model_selection import KFold
'''
This file is based on similar dataset handler by Milan Straka https://github.com/ufal/npfl114/blob/master/labs/08/morpho_dataset.py
However it has been extensively altered.

License: CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
'''


class CreditCardDataset:
    ''' Class which loads the credit card dataset in .npy format and provides suitable interface for tensorflow '''

    def __init__(self, data, dim=31, shuffle=True, seed=88):
        np.seed=seed
        self._data = data
        self._shuffle = shuffle
        self._dim = dim
        self._permutation = np.random.permutation(len(self._data)) if self._shuffle else np.arange(len(self._data))
        self._normal_indices = np.where(self._data[:,30] == 0.)[0]
        self._anomalous_indices = np.where(self._data[:,30] == 1.)[0]

    @classmethod
    def from_file(cls, path, dim=31, shuffle=True, seed=88):
        data = np.load(path)
        return cls(data, dim, shuffle, seed)

    @classmethod
    def from_array(cls, data, dim=31, shuffle=True, seed=88):
        return cls(data, dim, shuffle, seed)

    def next_batch(self, size):
        ''' Returns [size,features_size]  '''

        size = min(size, len(self._permutation))
        permutation = self._permutation[:size]
        self._permutation = self._permutation[size:]
        data = np.take(self._data[permutation], np.arange(1,self._dim-2), axis=1)
        return data

    @property
    def epoch_finished(self):
        if len(self._permutation) == 0:
            # This resultes in training on the same instance more than once
            self._permutation = np.random.permutation(len(self._data)) if self._shuffle else np.arange(len(self._data))
            return True
        return False

    def get_normal_samples(self, size, random=True):
        ''' Returns a random array of size `size` of normal instances '''

        size = min(size, len(self._normal_indices))
        permutation = self._normal_indices[:size] if not random else np.random.choice(self._normal_indices, size)
        data = np.take(self._data[permutation], np.arange(1,self._dim-2), axis=1)
        return data

    def get_anomalous_samples(self, size, random=True):
        ''' Returns a random array of size `size` of anomalous instances '''

        size = min(size, len(self._anomalous_indices))
        permutation = self._anomalous_indices[:size] if not random else np.random.choice(self._anomalous_indices, size)
        data = np.take(self._data[permutation], np.arange(1,self._dim-2), axis=1)
        return data

    def get_all_normal(self):
        return self.get_normal_samples(len(self._normal_indices), random=False)

    def get_all_anomalous(self):
        return self.get_anomalous_samples(len(self._anomalous_indices), random=False)

    def get_sample(self, size):
        size = min(size, len(self._normal_indices))
        permutation = np.random.choice(self._normal_indices, size)
        data = np.take(self._data[permutation], np.arange(1,self._dim-1), axis=1)
        return data

    def get_all(self):
        return (np.take(self._data, np.arange(1, self._dim-2), axis=1), self._data[:,30])

    @property
    def anomalous_size(self):
        return len(self._anomalous_indices)

    @property
    def normal_size(self):
        return len(self._normal_indices)

class CrossValidator:
    ''' Class serving for crossvalidation of model '''
    def __init__(self, path, dim=31, suffle=True, seed=88):
        self._data = np.load(path)
        self._normal = self._data[np.where(self._data[:,30] == 0.)[0]]
        self._anomalous = self._data[np.where(self._data[:,30] == 1.)[0]]

        self.kfold = KFold(n_splits=29, random_state=seed, shuffle=True)
        self.folds = self.kfold.split(self._normal)
        self.current = 0

    def get_next_fold(self):
        ''' returns tuple (CCD(train), CCD(test)) using k fold cross validation'''
        fold = next(self.folds)
        data1 = self._normal[fold[0]]
        data2 = np.concatenate((self._normal[fold[1]], self._anomalous))
        return (CreditCardDataset.from_array(data=data1), CreditCardDataset.from_array(data=data2))


if __name__=='__main__':
    cv = CrossValidator('./data.nosync/cross.npy')
    print('tra norm, tra anom, test nom, test anom')
    # for i in cv.folds:
        # print(
    x = cv.get_next_fold()
    print(x[0].get_all_normal().shape)
    print(x[0].get_all_anomalous().shape)
    print(x[1].get_all_normal().shape)
    print(x[1].get_all_anomalous().shape)
    print('')

    x = cv.get_next_fold()
    print(x[0].get_all_normal().shape)
    print(x[0].get_all_anomalous().shape)
    print(x[1].get_all_normal().shape)
    print(x[1].get_all_anomalous().shape)
    print('')
    x = cv.get_next_fold()
    print(x[0].get_all_normal().shape)
    print(x[0].get_all_anomalous().shape)
    print(x[1].get_all_normal().shape)
    print(x[1].get_all_anomalous().shape)
    print('')
