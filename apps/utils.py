#!/usr/bin/env python3 
import numpy as np
import collections
from sklearn import metrics

def sigmoid( x):
    return x
    # return x / (1 + np.abs(x))
def sigmoid2(x):
    return 1 / (1 + np.exp(-x))


def compute_precision_recall(y, scores, file=None):
    precision, recall, thresholds = metrics.precision_recall_curve(y, sigmoid2(scores))
    pr = metrics.average_precision_score(y, sigmoid2(scores))
    if file:
        np.savez(file, precision=precision, recall=recall, thresholds=thresholds, pr=pr)
        return pr, precision, recall, thresholds
    else:
        return pr, precision, recall, thresholds
class LossCounter:
    def __init__(self, low, high):
        self._deque_low = collections.deque(maxlen=low)
        self._deque_high = collections.deque(maxlen=high)
        self._low = low
        self._high = high
        self._sum_low = 0
        self._sum_high = 0
        self._count = 0

    def update(self, loss):
        self._sum_low += loss - (0 if self._count < self._low else self._deque_low.popleft())
        self._sum_high += loss - (0 if self._count < self._high else self._deque_high.popleft() - self._sum_low)
        self._deque_low.append(loss)
        self._deque_high.append(loss)
        self._count += 1

    @property
    def high_avg(self):
        return self._sum_high / (self._high - self._low) if self._count >= self._high else 0

    @property
    def low_avg(self):
        return self._sum_low / self._low if self._count >= self._high else 0


    def stop_train(self, treshold):
        return self.low_avg + treshold < self.high_avg
