\Title{Anomaly Detection Using Generative Adversarial Networks}
\Author{Ondrej Mekota}
\Keywords{anomaly detection\sep generative adversarial networks\sep neural network\sep deep learning}
\Subject{
Generative adversarial networks (GANs) are able to capture distribution of its inputs. They are thus used to learn the distribution of normal data and then to detect anomalies, even if they are very rare; e.g. Schlegl et al. (2017) proposed anomaly detection method called AnoGAN. However, a major disadvantage of GANs is instability during training. Therefore, Arjovsky et al. (2017) proposed new version, called Wasserstein GAN (WGAN).
The goal of this work is to propose a model, utilizing WGANs, to detect fraudulent credit card transactions. We develop new method called AnoWGAN+e, partially based on AnoGAN, and compare it with One Class Support Vector Machines (OC-SVM) (Sch\"olkopf et al. (2001)), k-Means ensemble (Porwal et al. (2018))  and other methods. Performance of studied methods is measured by area under precision-recall curve (AUPRC), and precision at different recall levels on credit card fraud dataset (Pozzolo (2015)). AnoWGAN+e achieved the highest AUPRC and it is~$12\%$ better than the next best method OC-SVM.
Our model has 20\% precision at 80\% recall, compared to 8\% precision of the next best method -- OC-SVM, and 89\% precision at 10\% recall as opposed to 79\% of k-Means ensemble.}
\Publisher{Charles University}
