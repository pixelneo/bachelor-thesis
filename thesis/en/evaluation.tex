\chapter{Evaluation} \label{chap:results}
This chapter presents results of performance measurements of \abw{} whose architecture is described in Section~\ref{section:model}.

\par In the first section of this chapter, we compare our model, both with encoder and without it, to~$k$-Means ensemble~\cite{Porwal2018}, \as{ocsvm} algorithm~\cite{Cortes1995}, and \al{if}~\cite{Liu2008}.
In the second section, we analyze received results.

\par \textbf{\abwe{} has the highest average \as{auprc}} of all compared methods,
see Table~\ref{tab04:fdr} and Figure~\ref{img05:results}.
Table~\ref{tab02:comparison} shows the difference in performance, measured by \as{auprc} and evaluation time of studied methods.
%ebay
%Table~\ref{tab04:fdr} compares precision of \abwe{} with \as{ocsvm} at different recall levels.
Table~\ref{tab04:fdr} compares precision of tested methods at different recall levels.

\section{Results}
We use area under precision-recall curve (\as{auprc}) and precision at various recall levels as evaluation metrics. 
It is important to keep the ratio of normal and anomalous instances almost the same during evaluation since PR curve depends on it.

\par 
\emph{Efficient AnoGAN} (in Table~\ref{tab02:comparison}) is the model proposed by Zenati~\cite{Zenati2018}, which we have not been able to stabilize, see Section~\ref{section:eff}.
We use it as a GAN baseline.

 
\begin{table}[h!]
\centering
\begin{tabular}{lrrr}
\toprule
Method                                  & Average \as{auprc} & Variance & Evaluation time (s)   \\
\midrule
\abwe{}                                 & \textbf{0.4625}    & 0.00378  & 9.96        \\
One Class SVM                           & 0.4113             & 0.00475  & \textbf{1.03}        \\
\abwm{}                                 & 0.2706             & 0.00320  & 433.10      \\
$k$-Means ensemble\footnotemark{}       & 0.2231             & 0.00144  & ---         \\
Isolation Forest                        & 0.1827             & 0.00261  & 2.27        \\
Efficient AnoGAN                        & 0.1196             & 0.00073  & 10.02       \\
%ebay
\bottomrule
\end{tabular}

\caption{Evaluation of compared methods.}\label{tab02:comparison}
\end{table}
%ebay
\footnotetext{Results for this method are taken from the paper~\cite{Porwal2018} proposing it. The variance is from 10 runs, each with different ordering of dataset.}

\par The dataset, with which we work, is highly imbalanced --- approximately 577 normal examples to 1 anomalous.
We have 9735 normal examples and 442 anomalous ones for testing.
To keep the ratio of normal and anomalous instances as close as possible to the original one, we make predictions on all the normal instances and 17 anomalous, arriving at ratio 572 to 1.
This is repeated 26 times for different anomalous instances and the area under PR curves is averaged.

\par We run 29-fold cross validation.
Each time the model is trained on a different subset of data and evaluated on a test set (disjoint from corresponding train set).
Sets of normal data of the test sets (from the 29 runs) are disjoint.
Resulting \as{auprc} is the average of \asp{auprc} from these runs.

\par Evaluation time has been measured on a sample consisting of 10117 examples.
The measurement is conducted on a personal computer with \as{cpu} Intel\textsuperscript{®} Core™ i7-6700. 
We have run five such evaluations and averaged the measured times, the variance of these runs is negligible: it is lower than one percent.

\par To illustrate the difference between studied techniques, we average precision from the 29 runs at each recall level, making precision-recall curve. Linear interpolation is used to smooth the curve~\ref{img05:results}.



\begin{figure}[H]\centering
%ebay
%\includegraphics[width=140mm]{../img/results}
\includegraphics[width=140mm]{../img/results2}
\caption{Graph of averaged precision-recall curves.}
\label{img05:results}
\end{figure}

\par \textbf{Our method (\abwe{}) achieved the highest precision of all methods} at recall levels~$0$ --~$0.2$ and~$0.6$ --~$0.9$, see table~\ref{tab04:fdr}.
%ebay
Although~$k$-Means ensemble has low \as{auprc}, its precision is better than \as{ocsvm}'s at lower recall levels but it has been outperformed by \abwe{} in every metric, see table~\ref{tab04:fdr} and figure~\ref{img05:results}.


\begin{table}[h!]
\small
\centering
\begin{tabular}{l|rrrrrrrrr}
\toprule
\backslashbox{Method}{Recall}   & 0.1  & 0.2 & 0.3 & 0.4 & 0.5 & 0.6 & 0.7 & 0.8 & 0.9 \\
\midrule
\abwe{} & \textbf{0.89} & \textbf{0.78} & 0.59 & 0.53 & 0.45 & \textbf{0.35} & \textbf{0.30} & \textbf{0.20} & \textbf{0.05} \\
\as{ocsvm} & 0.70 & 0.69 & \textbf{0.62} & \textbf{0.59} & \textbf{0.48} & 0.32 & 0.23 & 0.08 & 0.01 \\
\al{if} & 0.34 & 0.32 & 0.24 & 0.22 & 0.17 & 0.13 & 0.11 & 0.07 & 0.02 \\
\abwm{} & 0.62 & 0.51 & 0.34 & 0.31 & 0.20 & 0.14 & 0.12 & 0.07 & 0.03 \\
$k$-Means ensemble & 0.79 & 0.61 & 0.21 & 0.10 & 0.05 & 0.05 & 0.04 & 0.01 & 0.00 \\
%\abwe{}                             & 0.1205   & 0.1579  & 0.1956  & 0.2823        \\
%%ebay
%$k$-Means ensemble                  & 0.1817   & 0.2067  & 0.2271  & 0.3919      \\
%One Class SVM                       & 0.1910   & 0.2961  & 0.2999  & 0.3107      \\
\bottomrule
\end{tabular}

\caption{Precision at different recall levels.}\label{tab04:fdr}
\end{table}

In Figure~\ref{img06:repr}, there is a visualization of normal and anomalous data points mapped into latent space.

\begin{figure}[H]\centering
\includegraphics[width=140mm]{../img/repr_latent}
\caption{Visualization of mapped points (using encoder) to latent space. THe dimensionality of the latent space is reduced using \as{tsne}. There is no reasonable interpretation of axes.}
\label{img06:repr}
\end{figure}

\section{Discussion}
The model we propose, \abwe{}, has the highest average \as{auprc} from all compared methods, surpassing the state-of-the-art \as{ocsvm} (to the best of our knowledge).
It also outperforms other models in precision at some recall levels, especially from~$10\%$ to~$20\%$ and from~$70\%$ to~$80\%$.
Both of these intervals can be very useful for real world applications;
for example banks may want to detect a portion of credit card frauds with high level of certainty, where the interval from~$10\%$ to~$20\%$ comes into use (at~$10\%$ recall almost~$90\%$ of samples marked as anomalies actually are anomalies).

\par A model with high recall (but lower precision) also has an important place in fraud detection. Consider~$80\%$ recall, then our model detects anomalies with~$20\%$ precision.
When we take into account that there are 492 anomalies in the dataset, then this means that we can detect~$80\%$ of them by manually examining less than~$2000$ transactions. 

\par Porwal et al.~\cite{Porwal2018} claim they can ``identify~$40\%$ of fraud cases with high precision'' ($0.1$ precision). 
However, we outperform their method by identifying~$80\%$ of fraud cases even with double precision, see table~\ref{tab04:fdr}.
%They compare performance of their model with the work of Pozzolo et al.~\cite{Pozzolo2015} on \as{ccfd} and achieve higher \as{auprc}, which means our model outperforms~\cite{Pozzolo2015} either.

\abwm{} did not perform very well.
We tried running the evaluation with twice the number of iterations but the improvement of \as{auprc} was insignificant, considering doubling the evaluation time.
Its evaluation time is, as expected, slower than the times of the rest of the algorithms.

\par We believe that the difference in \as{auprc} of \abw{} with and without encoder is caused by the fact that encoder learns the mapping of normal examples very well and does not experience anomalous ones during training,
resulting in a bigger gap between the normal and the anomalous.
Whereas mapping each individual sample relies only on the generator.
%whereas mapping each individual sample is more likely to incorrectly classify anomaly as normal example.
In other words, the encoder compensates for imperfect generator.
We can see that \textbf{utilizing encoder to latent space is essential for achieving good results}. 

\par We feel obliged to say that the time measurements are probably influenced by the Python frameworks used by the compared methods – Scikit-learn~\cite{scikit-learn} and Tensorflow~\cite{Abadi2015}.
Some of the models might run faster on GPU that CPU, but we wanted the measurements to be comparable.


