\chapter{Experiments} \label{chap:experiments}
The focus of this chapter is the practical use of our model (\abw{}) to detect fraudulent credit card transactions.
In the first section, we explore the \al{ccfd}.
Section~\ref{section:model} describes the neural network, used as our model, and the training process.
Section~\ref{section:implementation} describes the implementation of models compared in the next chapter (\ref{chap:results}) and concrete hyperparameters necessary to reproduce our results.
Section~\ref{section:eff} is about failed attempt to train the \as{ali} model.
\section{Dataset}
There are not many publicly available datasets suitable for anomaly detection. 
Most publicly available datasets, such as \emph{Arrythmia} or \emph{Thyroid}~\cite{Dua2017}, do not have a sufficient number of instances for \as{gan} to be able to train itself on them.

\par Dataset\footnote{Credit Card Fraud Dataset is available at \url{https://www.kaggle.com/mlg-ulb/creditcardfraud} (visited: 04/07/2019)} 
which is used in this thesis is \af{ccfd} made available by Pozzolo et al.~\cite{PozzoloThesis, Pozzolo2015}.
There are 284\,315 normal examples and 492 anomalous ones.
This is suitable for the method of our choosing since we only need normal instances during training. 
\par The \as{ccfd} is anonymized, using \af{pca}~\cite{Jolliffe1986}, so that it could be published. 
Every data point in \as{ccfd} consists of 30 real numbers, e.g. amount; time measured from the first transaction. 
For the purpose of our task, we omit the first and last feature (time and amount).

\begin{table}[h!]
\centering
\begin{tabular}{lrrrrr}
\toprule
\# &        0  &        1  &      \dotso  &        28 &      29 \\
\midrule
0 &   36462 &  0.774650 & \dotso  &  0.010446 &  118.80 \\
1 &  172213 &  2.113557 & \dotso  & -0.038202 &    1.29 \\
2 &  127823 &  1.933172 & \dotso  & -0.037490 &   70.00 \\
3 &  158628 &  1.800412 & \dotso  & -0.023028 &  150.00 \\
4 &   73543 & -0.620267 & \dotso  & -0.342146 &    4.52 \\

\bottomrule
\end{tabular}

\caption{Sample of the dataset. Each line is one instance (transaction) processed by \as{pca}. First column is time and the last one is amount.}\label{tab01:sample}
\end{table}

\par In Figure~\ref{img08:corr} is the correlation matrix of the remaining features.
We randomly sampled 2000 normal instances and 50 anomalous to perform hyperparameter search.
The rest of the data is used for training and testing.
%Test set contains 10000 normal examples and rest of the anomalies (442).
%Train set consists of the remaining normal samples.

\begin{figure}[H]\centering
\includegraphics[width=110mm]{../img/corr}
\caption{Correlation of used features.}
\label{img08:corr}
\end{figure}

\section{AnoWGAN } \label{section:model}
In this section, we discuss the model used to detect anomalies in the \al{ccfd}.
We developed two versions of the model -- \abwm{} and \abwe{}. Both of them use Improved \as{wgan}~\cite{Gulrajani2017} to learn the distribution of normal instances.
They differ in mapping to latent space during evaluation -- the first model uses iterative mapping and the other trained encoder, see Section~\ref{section:anobiwgan}.

\subsection{Learning normal data distribution} \label{section:normal}
We use a fully connected neural network in both generator and critic. 
The overall architecture of \abw{} used to learn  normal data manifold is depicted in Figure~\ref{img03:model}.
The generator is slightly \emph{weaker}, consisting of a smaller number of layers, than the critic as we do need to have a very solid critic, 
one that is able to "criticize" the generated instances as good as possible.

\par After all hidden layers, there is \emph{Leaky ReLU} activation function. 
ReLU is abbrevation for \emph{rectified linear unit}, defined as
\begin{equation*}
ReLU(x) = max(0,x).
\end{equation*}

Leaky ReLU is parametrized function similar to ReLU with the difference that it allows a small gradient for input values smaller than~$0$.
\begin{equation}
    Leaky ReLU(x; \alpha)= 
        \begin{cases}
            x, & \text{if } x \geq 0\\
            \alpha x, & \text{otherwise}
        \end{cases}
\end{equation}
where~$\alpha \in (0,1)$.

\par After most of the layers (and their activations), there is a dropout~\cite{Srivastava14a}. 
Dropout ensures that each parameter of the layer is excluded from training (at given time step) with some desired probability and therefore forcing the network to train all the connections between layers.
In our case, the probability is~$0.2$ for the generator layers and~$0.3$ for critic layers.

\begin{figure}[H]\centering
\includegraphics[width=140mm]{../img/model}
\caption{Diagram of our model. The numbers under dense layers denote their output sizes. The numbers under latent and data space denote the dimensionality.}
\label{img03:model}
\end{figure}

\par Latent space dimension is~$36$ when the encoder is utilized, and~$12$ when we use only mapping. 
Instead of using uniform random distribution for sampling as in~\cite{Gulrajani2017}, we use normal distribution for it helps the model to converge.

\par The training procedure for learning dataset distribution can be seen in Algorithm~\ref{training}.

\subsection{Detecting anomalies} \label{section:anomaly}
In the previous subsection, we described a model which learns the distribution of normal instances.
Now we would like to use that distribution to test whether a data point is an anomaly or not.
We use two methods to do that.

\par First is iterative \emph{mapping} to latent space which was proposed by Schlegl et al.~\cite{Schlegl2017}, see Section~\ref{section:anogan}.
The second approach we chose, is to train another neural network which learns the mapping from data space~$p_{data}$ to latent space~$\latent$, see Section~\ref{section:anobiwgan}

\begin{figure}[H]\centering
\includegraphics[width=140mm]{../img/encoder}
\caption{Diagram of the encoder. The rest of the network (generator and critic) is the same as in Figure~\ref{img03:model}}
\label{img04:encoder}
\end{figure}



\par The architecture of encoder used to detect anomalies in \as{ccfd} and its place within the whole network is depicted in Figure~\ref{img04:encoder}.

\subsection{Training and hyperparameters}
In this subsection, we describe the training process and hyperparameters of both the model used to learn the distribution of the data and the encoder to latent space.

\subsubsection{Generator and critic}
At the start of the training, we pre-train the critic using~$2000$ update steps. 
Unlike training regular \asp{gan}, where it is desirable to balance training of generator and discriminator, in \asp{wgan} on the other hand we want the critic to be well trained at every stage.
We find this necessary because even in the beginning of the training the generator needs to have a good ``feedback'' from the critic on the samples it generates, and Wasserstein distance makes it possible for the critic to provide usable gradients even if it is much ``better'' than the generator.

The initial learning rate of the generator is set to \glr{} and critic starts with \clr{}.

\begin{lstlisting}[float,floatplacement=h, caption={Pseudocode of training the model to learn the representation of normal instances.},label=training]
# c_lr is critic learning rate
# g_lr is generator learning rate

train critic 2000 times
for e in epochs:
    # lower learning rate every two epochs
    if e > 0 and e is even: 
        c_lr /= 1.25
        g_lr /= 1.65
        
    # pretrain critic and generator
    train critic 1000 times
    train generator 30 times

    while e has not ended:
        # critic iterations increase with epoch
        for c in 1...(7 + 2*e):
            take batch of size 4
            train critic 

        train generator
\end{lstlisting}

\par We train our model for \epochs{} epochs on batches of size \batch{}.
At the start of each epoch, we perform \cstart{} update steps of the critic and \gstart{} update steps of the generator.
After every other epoch, we divide the learning rate of the generator and the critic by 1.65 and 1.25 respectively. 


\par For every update of the generator, we update the critic seven times. 
This is necessary; otherwise, if we would do one update of critic to one update of generator, the generator would become too good with respect to the critic.
And it would be easy for the generator to generate samples which the critic would not be able to distinguish from the real samples; however, the generated instances would not be of a good quality (as the critic itself would be weak).
Every epoch, the number of critic iterations is increased by two.
The training procedure is displayed in Algorithm~\ref{training}.


\subsubsection{Encoder} \label{section:encoder}
Keeping the parameters of the generator fixed, we train the encoder for \epochse{} epochs with mini-batches of size \batche{}.
The critic is not used at all during encoder training.
The initial learning rate is set to \elr{} and is also decayed, but this time every four epochs by factor of~$0.5$.

\begin{lstlisting}[float,floatplacement=h, caption={Pseudocode of encoder training.},label=encoder_training]
# e_lr is encoder learning rate
restore generator

for e in encoder_epochs:
    # lower learning rate every four epochs
    if e > 0 and e modulo 4 == 0:
        e_lr /= 2
        
    while e has not ended:
        take batch of size 32
        train encoder
\end{lstlisting}

\par We use \af{tsne}~\cite{Maaten2008} to visualize the learned manifold~\ref{img07:manifold}.
Algorithm \as{tsne} is mostly used for dimensionality reduction with the intention to visualize the data in two or three-dimensional space.

\begin{figure}[H]\centering
\includegraphics[width=140mm]{../img/manifold}
\caption{Visualization of learned manifold using t-SNE. Twenty thousand \emph{training} samples are mapped to the latent space through the encoder to make this image. There is no reasonable interpretation of axes.}
\label{img07:manifold}
\end{figure}

\section{Implementation} \label{section:implementation}
We used a computer with Intel\textsuperscript{®} Core™ i7-6700 \as{cpu} and Gentoo\footnote{\url{https://www.gentoo.org} (visited 04/28/2019)} operating system to run the experiments.

\par The model is implemented in Tensorflow 1.12~\cite{Abadi2015}.
Dataset, which is available in comma separated value format, is converted to \cd{.npy} format, which is used by Python library NumPy 1.15.4~\cite{numpy} for storing numerical arrays.
We ran the model in Python 3.6.5.
Methods, to which we compare our model (\al{ocsvm} and \al{if}), are already implemented in Scikit-learn 0.20.2~\cite{scikit-learn} library for Python. 
The implemented model is an attachment to this thesis, see Section~\ref{section:attach}. 
\par In the next subsections, we describe all hyperparameters used during training of baselines and our model.

\subsection{Hyperparameters of baselines}
%ebay
We use \as{ocsvm},~$k$-Means ensemble and \al{if} to compare our model with.
The results of~$k$-Means ensemble are taken from the original paper~\cite{Porwal2018}.
We have implemented the model but the results we have got were inferior to those in the paper (training is sensitive to the order in which the data points are presented~\cite{Porwal2018}), so we decided not to use our implementation and state the results from the paper.

\par \as{ocsvm} has hyperparametes used in kernel\footnote{\url{https://www.kaggle.com/neoyipeng2018/one-class-svm-and-data-leakage} (visited: 04/08/2019)} on Kaggle website. 
Hyperparameters of \al{if} are also from a kernel\footnote{\href{https://www.kaggle.com/rgaddati/unsupervised-fraud-detection-isolation-forest}{\texttt{https://www.kaggle.com/rgaddati/unsupervised-fraud-detection-isolation\--forest}} (visited: 04/15/2019)} on Kaggle website, from which the \as{ccfd} comes.
As the model is implemented in Scikit-learn 0.20.2~\cite{scikit-learn} we provide the hyperparameters as a constructor of \as{ocsvm} class in Scikit-learn, see algorithms~\ref{ocsvm_alg}, \ref{if_alg}.

\begin{lstlisting}[float,floatplacement=H!, caption={Constructor for OC-SVM class in Scikit-learn.},label=ocsvm_alg]
OneClassSVM(coef0=0.0, degree=3, \
            gamma=0.007, kernel='rbf', \
            max_iter=-1, nu=0.0005, \
            random_state=41, shrinking=True, \
            tol=0.001)
\end{lstlisting}

%\par \al{if} uses hyperparameters shown in algorithm~\ref{if_alg}.
%They are the same as in kernel\footnote{See footnote~\ref{kagglekernel}} on Kaggle website.
\begin{lstlisting}[float,floatplacement=!, caption={Constructor for Isolation Forest class in Scikit-learn.},label=if_alg]
IsolationForest(n_estimators=100, max_samples='auto', \
                contamination=0.01, max_features=1.0, \
                bootstrap=False, n_jobs=-1, \
                random_state=41, verbose=0, \
                behaviour="new")
\end{lstlisting}

\subsection{Hyperparameters of AnoWGAN}
Hyperparameters used for training and evaluation of our model on the \al{ccfd} dataset are presented in Table~\ref{tab03:hyperparams}.
Furthermore, training uses the following hyperparameters.

\begin{itemize}
    \item After every other epoch, we divide the learning rate of the generator and the critic by~$1.65$ and~$1.25$ respectively.
    \item The encoder learning rate is divided by~$2$ every four epochs.
    \item At the start of the training, the critic is pre-trained on~$2000$ training steps.
    \item Starting from the second epoch, the critic and the generator are pre-trained on~$1000$ and~$30$ training steps respectively.
    \item The number of critic iterations is increased by~$2$ every epoch.
    \item At the end of the last epoch, during normal data distribution learning, the generator is trained for~$10$ more steps.

\end{itemize}

\begin{table}[H]\centering
\begin{tabular}{lrrrrr}
\toprule
Hyperparameter &            value                                       \\
\midrule
Batch size &                                ~$4$                        \\
Batch size – encoder &                      ~$32$                        \\
Initial learning rate G &                   ~$3.0 \cdot 10^{-5}$          \\
Initial learning rate D &                   ~$10^{-4}$                     \\
Initial learning rate E &                   ~$10^{-5}$                    \\
Initial critic iterations &                 ~$7$                         \\
Epochs &                                    ~$5$                         \\
Epochs – encoder &                          ~$6$                         \\
Penalty coefficient~$\lambda$ &             ~$12$                         \\
Mapping coefficient~$\zeta$ &               ~$0.0$                        \\
Dropout – generator&                        ~$0.2$                         \\
Dropout – critic&                           ~$0.3$                         \\
Latent space dimensionality -- encoder&                ~$36$                         \\
Latent space dimensionality -- mapping&                ~$12$                         \\
Mapping iterations&                         ~$70$                         \\
Mapping learning rate&                      ~$0.09$                       \\
Adam first momentum~$\beta_1$&              ~$0.1$                          \\
Adam second momentum~$\beta_2$&             ~$0.9$                          \\
\bottomrule
\end{tabular}
\caption{Hyperparameters used during training and evaluation.}
\label{tab03:hyperparams}
\end{table}

\par When splitting dataset to train, development and test parts, we use random seed 42 (for NumPy).
Tensorflow uses random seed 88, so does NumPy when sampling the dataset.
When evaluating we set NumPy random seed to 42.

\section{Efficient AnoGAN}\label{section:eff}
We have tried to implement anomaly detection technique from~\cite{Zenati2018}.
However, BiGANs showed themselves to be very unstable during training and we have not been able to stabilize it on the \as{ccfd}.

\par In Figures~\ref{img09:bigan_gen} and~\ref{img10:bigan_dis} we show loss functions for different sets of parameters.
These three models differ in the number of the discriminator iterations (how many training steps discriminator takes per one generator step) and batch sizes.
All models have learning rates of both the discriminator and the generator set to~$10^{-5}$, which was empirically chosen.
The models were trained for~$10$ epochs.

\par Though higher batch size leads to lower variance of the loss, it is obvious the loss functions do not show signes of convergence; for the generator tries to minimize the same loss function the disciminator maximizes.
It is natural that at the beginning of the training losses of generator and discriminator are symmetrical and do not converge for a while. 
However, unlike what we observe in the Figures~\ref{img09:bigan_gen} and~\ref{img10:bigan_dis}, we would expect the loss functions to start converging and reach \emph{Nash} equilibrium~\cite{Mescheder2018}. 
It is however not proven that it will converge, moreover it  has been proven that in some cases (on some types of data) \asp{gan} do not converge~\cite{Mescheder2018}.


\begin{figure}[H]\centering
\includegraphics[width=140mm]{../img/bigan_gen}
\caption{BiGAN generator loss.}
\label{img09:bigan_gen}
\end{figure}

\begin{figure}[H]\centering
\includegraphics[width=140mm]{../img/bigan_dis}
\caption{BiGAN discriminator loss.}
\label{img10:bigan_dis}
\end{figure}



