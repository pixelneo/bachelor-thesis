---
documentclass: extarticle
fontsize: 17pt
fontfamily: arev
linestretch: 1.5
geometry:
- margin=1.0cm 
---
\pagenumbering{gobble}
# Slide 1
Dobrý den,
Budu mluvit o detekci anomálií pomocí generativních adversariálních sítí.
\pagebreak

# Slide 2 – Intro
- Detekce anomálií je důležitým tématem v průmyslu.
    * Banky potřebují detekovat podvodné transakce kreditními kartami.
    * Internetové společnosti potřebují rozlišovat mezi legitimními HTTP dotazy a mezi útoky na jejich web.
    * Ve zdravotnictví a v lekářském výzkumu má detekce anomálií uplatnění v diagnostice pacienta – zda je nemocen, případně jakou nemoc má. 
- Protože anomálie jsou často velmi řídké a je težké naučit se jejich distribuci, 
rozhodli jsme se zaměřit se naopak na normální data, kterých často pozorujeme mnoho.
- Anomálie potom detekujeme jako příklady, výrazně se lišící od naučené distribuce.
- Pro naučení se distribuci normálních příkladů se hodí generativních adversariální sítě.
- Cílem této práce je navrhnout model na nich založený a použít ho pro detekci podvodných transakcí kreditními kartami. 

\pagebreak

# Slide 3 – Outline
- V první části stručně vysvětlím, co to jsou generativní adversariální sítě
- V další části popíšu námi navrhnutý model.
- A v poslední části budu mluvit o výsledcích porovnání našeho modelu s dalšími metodami.

\pagebreak

# Slide 4 – generative adver nets 
- Generativních adversariální sítě jsou třídou algoritmů strojového učení.
- Mají dvě komponenty: Generátor and Diskriminátor, které pracují proti sobě.
    * Generátor se snaží generovat příklady co nejvíce podobné reálným datům.
    * A diskriminátor se pokouší rozlišit vygenerované příklady a ty, co pocházejí z reálné distribuce. 
- V posledních letech generativní adversariální sítě byly intensivně studovány.  
Převážně se používají pro generování "umělých" fotografií. 

**[OBRÁZEK]**

- Na obrázku můžeme vidět obecnou architekturu generativní adresariální sitě. Generátor i diskriminátor je neuronová síť.
    * náhodně vybereme vektor z latentního prostoru
    * potom generátor vygeneruje příklad na základě vybraného vektoru tak, že maximalizuje svůj výstup discriminátoru. 
    * diskriminátor potom udělá dvě věci: za prvé, vezme příklad z reálné distribuce a maximalizuje na něm svůj výstup a za druhé, dostane vygenerovaný příklad, na kterém naopak svůj výstup minimalizuje.

\pagebreak

# Slide 5 – Wasserstein GAN
- GANy mají skvělé výsledky v generování umělých obrázků, ale stále trpí řadou problémů
- Jedním z nich je nestabilita behěm trénování.
- Je důležité trénovat generátor i discriminátor se stejnou intensitou tak, aby ani jeden z nich nebyl příliš silný a ten druhý byl schopen se trénovat.
- Wassestein GAN je metoda zlepšující stability trenování
- Kritik, což je přejmenovaný diskriminátor, je schopen poskytovat použitelné gradienty pro generátor i když je generátor nedostatečně natrénovaný.

**[OBRÁZEK]**

- Na obrázku jsou vidět ztrátové funkce (loss funkce) diskriminátoru GANu a kritika Wasserstein GANu. 
- Je vidět, že když je diskriminátor velmi silný tak se gradienty jeho ztrátové funkce blíží na většině definičního oboru nule. 
- Naopak critic u WGAN si je schopen zachovat použitelné gradienty skoro všude.


\pagebreak


# Slide 6 - AnoWGAN+e - normal data
- Naše metoda je natrénována pouze na normálních, neanomálních příkladech.
<!--- Trénování je rozděleno na dvě části: naučení se distribuce normálních dat, a naučení se mapování z prostoru dat do latentního prostoru.-->
- Na začátku použijeme Wasserstein GAN s gradient penalty pro naučení distribuce normálních dat.
- Díky Wasserstein GANu je možné mít silnějšího kritika, používajícího více vrstev, než generátor.  
Je to dokonce žádoucí mít co nejsilnějšího kritika.
- Abych byli schopni detekovat anomálie potřebujeme se naučit mapování z prostoru dat do latentního prostoru.  
K tomu použijeme další neuronovou síť – encoder.
\pagebreak

# Slide 7 - AnoWGAN+e - encoder
- Encoder mapuje body z prostoru dat do latentního prostoru.
- Je natrénován na stejných datech jako generátor a kritik,  
ale až potom, co trénování generátoru a kritika skončilo.
- Cílem encoderu je minimalizovat rozdíl mezi reálnými příklady a příklady, které byli první "zakódovány" do latentního prostoru a potom "dekódovány" generátorem.

\pagebreak

# Slide 8 - Results - AUPRC table
- Porovnali jsme náš model s Isolation Forest, One Class Support Vector Machine a k-Means ensemble.  
**[TADY je tabulka]**
- Náš model dosáhl nejlepho výsledku ze všech porovnávaných modelů, při použití metriky plocha pod precision-recall křivkou, na datasetu transakcí kreditními kartami.
\pagebreak

# Slide 9 - Results - recall table
- K porovnání metod jsme také využili přesnost na různých úrovních recallu.
- Náš model dosáhl největší přesnosti na úrovni recallu 0.1 – 0.2 a 0.6 – 0.9

\pagebreak

# Slide 10 - Results - graph


# Notes
- Lipschitz continuity means: $\|f(x_1) - f(x_2)\| \leq K\|x_1 - x_2\|$
- $p_{\hat{x}}$ is distribution sampled along $x = \epsilon x + (1-\epsilon) G(z)$


















