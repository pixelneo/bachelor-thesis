---
documentclass: extarticle
fontsize: 17pt
fontfamily: arev
linestretch: 1.5
geometry:
- margin=1.0cm 
---
\pagenumbering{gobble}
# Slide 1
Good morning,
I will be talking about anomaly detection using generative adversarial networks.
\pagebreak

# Slide 2 – Intro
- Anomaly detection is an important topic in industry.
    * Banks need to detect fraudulent credit card transactions
    * Internet companies have to distinguish credible HTTP requests from those intended to harm the website.
    * In medical research, doctors are trying to establish whether a patient is ill or healthy.
- In all of these areas anomaly detection comes into use, as in all of them the anomalous component – frauds, attacking packets, and ill patients.
- Goal of this thesis is to develop a model based on generative adversarial networks and use it to detect anomalies.

\pagebreak

# Slide 3 – generative adversarial networks
- Generative adversarial network is a machine learning algorithm.
- It has two components: Generator and Discriminator, which work against each other.
    * Generator tries to generate samples resembling the real data as close as possible.
    * And discriminator tries to distinguish whether supplied example has been is artificial or whether it comes from the real data.
- In recent years, generative adversarial networks have been intensively studied 
 which are predominantly used for generation of artificial images.

*[TADY JE OBRÁZEK]*

- On the picture, we can see a general achitecture of generative adversarial network.
    * we sample a vector from latent space $p_z$
    * then generator generates a sample in distribution $p_{model}$ such that it maximizes the discriminator's output
    * discriminator does two things: it takes a sample from real data $p_{data}$ and maximizes its output on it   
then it takes a the generators output and minimizes its output.

\pagebreak

# Slide 4 – Wasserstein GAN
- GANs can produce state-of-the-art results but they still have many problems.
- One of which is instability during training.
- It is important to train generator and discriminator with same intensity.  
Otherwise, one of them becomes too strong, and the other one is unable to train.
- Wassestein GAN is a new method trying to overcome these issues.
- Critic (which is a discriminator equivalent) in Wasserstein GAN, does not have problem of vanishing gradients even when the generator is undertrained.

\pagebreak


# Slide 5 - AnoWGAN+e - normal data
- Method we propose is trained only on normal data.  

- The training of our model is split into two parts:  learning normal data distribution and learning the mapping to latent space. 

- First, we use Wasserstein GAN with gradient penalty to learn the distribution of normal data, i.e. The ability to generate artificial normal examples.
- It is possible to have stronger critic, using more layers, due to the use of Wasserstein GAN. 
- GAN learns the manifold on which normal examples lie.  
This means that the generator is not able to generate anomalies.
- To detect whether tested sample is an anomaly, we need to find its representation in latent space.  
For that purpose we utilize another neural network – encoder.
\pagebreak

# Slide 6 - AnoWGAN+e - encoder
- Encoder maps point from data space to latent space
- It is trained on the same data as the generator and the critic are;  
however, after their training has been concluded.
- Its objective function is to minimize the dissimilarity of a real example and its image when mapped through encoder and generator.

\pagebreak

# Slide 7 - Results - AUPRC table
- We have compared our GAN-based method with Isolation Forest, One Class Support Vector Machine and k-Means ensemble.  
*[TADY je tabulka]*
- Our model has been able to outperform them all in AUPRC on credit card fraud dataset.
\pagebreak

# Slide 8 - Results - recall table
- We also compare the methods using precision at different recall levels.
- Our model achieves the highest precision at recall levels 0.1 – 0.3 and 0.7 – 0.9

\pagebreak

# Slide 9 - Results - graph


# Notes
- Lipschitz continuity means: $\|f(x_1) - f(x_2)\| \leq K\|x_1 - x_2\|$
- $p_{\hat{x}}$ is distribution sampled along $x = \epsilon x + (1-\epsilon) G(z)$


















