This repository contains documents used in my bachelor thesis.
**This is still work in progress, some results may change!**
[Working version in PDF](https://www.ms.mff.cuni.cz/~mekotao/thesis.pdf)

# Anomaly detection using Generative Adversarial Networks
We propose AnoWGAN+e, a model for anomaly detection. Our work is partially based on architecture proposed by [Schlegl] et al. where we switched vanilla GAN for WGAN.
However, we also use *encoder* to the latent space which shows better results than the iterative mapping.
AnoWGAN+e exceeds all compared model in AUPRC and in precision at majority of the recall levels.

## Anomaly score 
We measure the performance by the area under precision-recall curve as the dataset is highly imbalanced (approx 570:1).
![results](apps/results.png)

The results can be reproduced using scripts in folder *implemetation.nosync*. Instruction on how to use them appear after running `make`.

[Schlegl]: https://arxiv.org/abs/1703.05921
